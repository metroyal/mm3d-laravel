<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MM3D Arsitektur | Architectural Visualization</title>
        <meta name="title" content="MM3D Arsitektur | Architectural Visualization">
        <meta name="description" content="MM3D Arsitektur is a 3D Architectural Visualization Service based in Solo City, Indonesia. You get the best quality with the best budget.">

        <!-- Open Graph / Facebook -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://www.mm3darsitektur.com/">
        <meta property="og:title" content="MM3D Arsitektur | Architectural Visualization">
        <meta property="og:description" content="MM3D Arsitektur is a 3D Architectural Visualization Service based in Solo City, Indonesia. You get the best quality with the best budget.">

        <!-- image -->
        <meta property="og:image" content="https://mm3darsitektur.com/storage/carousel/1722616499.jpg">

        <!-- Twitter -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:url" content="https://www.mm3darsitektur.com/">
        <meta name="twitter:title" content="MM3D Arsitektur | Architectural Visualization">
        <meta name="twitter:description" content="MM3D Arsitektur is a 3D Architectural Visualization Service based in Solo City, Indonesia. You get the best quality with the best budget.">
        <meta name="twitter:image" content="https://mm3darsitektur.com/storage/carousel/1722616499.jpg">
        <meta name="twitter:creator" content="@ak_rocks" />
        <meta name="author" content="Abdul Kadir">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon.ico') }}">
        <link href="{{URL::asset('css/app.css')}}" rel="stylesheet">
        <title>@yield('title')</title>

        <!-- GA -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-80655368-2"></script>
        <script type="text/javascript">
            if (document.location.hostname == "mm3darsitektur.com" || document.location.hostname == "www.mm3darsitektur.com") {
                // Google Tag Manager
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-K99S9QJ');
                // End Google Tag Manager

                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-80655368-2');
            } else {
                console.log('ini adalah localhost --> ', document.location.hostname);
            }
        </script>

        <!-- Bootstrap core CSS -->
        <!-- <link href="{{URL::asset('css/bootstrap.css')}}" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        
        <!-- Custom styles for this template -->
        <link href="{{ URL::asset('css/carousel.css') }}?v=1.2" rel="stylesheet">
        <!-- <link rel="stylesheet" href="{{URL::asset('css/responsive-nav.css')}}" type="text/css" media="screen" charset="utf-8"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="{{ URL::asset('css/animation.css') }}?v=1.0" type="text/css" media="screen" charset="utf-8"> -->
        @yield('css')

    </head>
    <!-- NAVBAR
    ================================================== -->
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K99S9QJ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        @yield('content')
        <!-- import app.js -->
        <!-- <script src="{{ asset('js/app.js') }}"></script> -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <script src="https://code.jquery.com/jquery-3.7.1.min.js?v=1.0"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{URL::asset('js/scrollReveal.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <!-- <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582Ltpw5OIinlRqscQCW7mTEJ2xqQFwhTvkogvV3TRrXRpVJ%2bE%2fG4T5X%2flW9UjzuDpXjsdf7SSJYBUTNDbc8ZC1J%2ftIfun4%2fSEZur3jGORcKupKdtAQgb1Ku8bB0NlJEvfZ4Mlb3A6WXI8xUrIHOyJSqbK8M48JJxwqvUtnLipYWWa2LqMqReeM1TowfriMECI011J4TFGq%2b9PWLzHTcGyHKZKzzNtpOuqKJSK17Fhurl1%2b7bYd5WzaToSAzodqo9V%2fzmjyKzlr9nC3kr9EHAYXY8HP4fiydlZcjaOKOQ2QSJceW9zHB1RYkw4n0xydzP%2bNE3B%2fe17e4MF6vp9k59KuvZwufFmRbuABaJXDwsgkcuGmXaeyh6B5Jc7b8UqKwMdHUDlLzBIKA%2bXHGCs5HMF5RdjPouM6t9qPWjM6XCPBYKSltjGf7%2fK8pRnZwa2eCEBAJS2Sv8MIVOMcIuEWdOMZ7AN6EihmdaNfzdtXdtJPUyu2%2bhh1d4O22EFU%2feBHF3eDpodG7hC05jr97CWdZC6plU%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script> -->
        <!-- <script src="{{URL::asset('js/bootstrap.min.js')}}"></script> -->
        <!-- <script src="{{URL::asset('js/responsive-nav.js')}}" type="text/javascript" charset="utf-8"></script> -->
        <!-- <script src="{{URL::asset('js/headerchangecolor.js')}}" type="text/javascript" charset="utf-8"></script> -->
        <script src="{{URL::asset('js/isotope-docs.min.js')}}" type="text/javascript" charset="utf-8"></script>
        <!-- <script src="{{URL::asset('js/jQueryhoverAnimation.js')}}" type="text/javascript" charset="utf-8"></script> -->
        <!-- sweet alert 2 -->
        <script>
            window.sr = new scrollReveal();
            // var nav = responsiveNav(".nav-collapse");
        </script>
        @yield('js')
        <!--Parallax Script
        =============================================================-->
        <script src="{{URL::asset('js/skrollr.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('js/imagesloaded.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('js/_main.js')}}" type="text/javascript"></script>
        <!-- @include('notification') -->
    </body>
</html>