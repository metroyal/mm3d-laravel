@extends('template')

@section('title')
    MM3D - Home
@stop

@section('css')
<link rel="stylesheet" href="{{URL::asset('css/style.css?v=1.0')}}" type="text/css" media="screen" charset="utf-8">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet" />
<!-- <link rel="stylesheet" href="{{URL::asset('css/ekko-lightbox.css')}}" type="text/css" media="screen" charset="utf-8"> -->
<style>
	.grid {
		display: flex;
		flex-wrap: wrap;
		gap: 10px; /* Space between flex items */
	}

	.grid-item {
		flex: 1 1 100%; /* Full-width on mobile */
		max-width: 100%; /* Ensure full width on mobile */
		box-sizing: border-box; /* Include padding and borders in the width */
	}

	/* Two-column layout on tablets (screen width 768px and above) */
	@media (min-width: 768px) {
		.grid-item {
			flex: 1 1 calc(50% - 10px); /* 2 columns on tablets */
			max-width: calc(50% - 10px);
		}
	}

	/* Three-column layout on desktops (screen width 992px and above) */
	@media (min-width: 992px) {
		.grid-item {
			flex: 1 1 calc(33.33% - 10px); /* 3 columns on desktops */
			max-width: calc(33.33% - 10px);
		}
	}

</style>
@endsection

@section('content')
	<!-- <div id="home" class="navbar-wrapper">
		<div class="main-header darken-menu">
			@include('layouts.nav')
		</div>
	</div> -->
	@include('layouts.newnav')
	<!-- Carousel ================================================== -->
	<div id="myCarousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="3000" data-bs-pause="false">
		<?php $i = 0; ?>
		<!-- Indicators -->
		<div  class="carousel-indicators">
			@foreach ($carousels as $carousel)
			<button type="button" data-bs-target="#myCarousel" data-bs-slide-to="<?php echo $i; ?>" class="{{ $loop->first ? 'active' : '' }}" aria-label="Slide {{ $i+1 }}" {{ $loop->first ? 'aria-current=true' : '' }}></button>
			<?php $i++; ?>
			@endforeach
		</div>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			@foreach ($carousels as $carousel)
			<div class="carousel-item animated {{ $loop->first ? 'active' : '' }}">
				<img src="{{ asset('/storage/carousel/'.$carousel->url_img) }}" class="d-block mx-auto" alt="carousel">
			</div>
			<!--  -->
			@endforeach
		</div>

		<!-- Controls -->
		<a class="carousel-control-prev" href="#myCarousel" role="button" data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Previous</span>
		</a>
		<a class="carousel-control-next" href="#myCarousel" role="button" data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Next</span>
		</a>
	</div><!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

	<!-- Three columns of text below the carousel -->
	<div id="service" class="homeSlide py-5">
		<section id="focus" class="focus bcg"
		data-bs-bottom-top="background-position:50% 230%;"
		data-bs-top-bottom="background-position:50% -100%;"
		data-bs-anchor-target="#parallax">
			<div class="container">
				<div class="row hsContainer">
					<div class="section_title">
						<h1>Our Services</h1>
					</div>
					<h2 class="passage">
						Our work is focused in bringing your design into an awesome architectural 3D Rendering for architects, designers, real estate developers and marketing agencies around the world. We provide still image, walkthrough animation, 360 panoramic rendering and interactive virtual tour to give your client an amazing virtual reality experience.
					</h2>
					<div class="col-lg-4 service float-shadow" data-sr="enter left, move 200px, over 1s">
						<h2>Still Images</h2>
						<span class="icon">
							<i class="fas fa-images"></i>
						</span>
						<p>We creates your design project into high quality and photorealistic 3D images, it includes interior, exterior, landscapes and any architectural projects.</p>
					</div>
					<div class="col-lg-4 service float-shadow" data-sr="enter left, move 100px, hustle 40px, over 1s">
						<h2>3D Animation</h2>
						<span class="icon">
							<i class="fas fa-play-circle"></i>
						</span>
						<p>3D Animation is the most effective marketing tool for architects, and property developers to attract consumers and give them a visual understanding of the whole project development.</p>
					</div>
					<div class="col-lg-4 service float-shadow" data-sr="enter right, move 200px, over 1s">
						<h2>360 Virtual Tour</h2>
						<span class="icon">
							<i class="fas fa-street-view"></i>
						</span>
						<p>Impress your client with an amazing interactive 360° images and virtual tours with your mobile device or desktop computer, all without leaving your web browser and no additional application needed.</p>
					</div>
				</div><!-- /.parallaxinside1 close -->
			</div><!-- /.container parallax1 close -->
		</section>
	</div><!--#Service Close-->

	<div id="works" class="homeSlide py-5">
		<div id="focus" class="focus2">
			<section>
				<div class="project">
					<div class="py-5">
						<div class="section_title">
							<h1>Works</h1>
						</div>
						<div class="container">
							<div class="row work-list">
								<ul id="filter" class="button-group filters-button-group">
									@foreach ($categories as $category)
									<li class="custom-navbar-categories @if($loop->first) selected @endif">
										<button data-filter=".{{ $category->class }}">{{ $category->category_name }}</button>
									</li>
									@endforeach
								</ul>

								<div class="grid"> <!-- This is now a grid container -->
									@foreach ($projects as $project)
									<div class="grid-item {{ $project->class }}" style="padding: 10px;">
										@php
											$url = $project->youtube_link; 
											$regex = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
											preg_match($regex, $url, $match);
											$isImage = $project->category_type == 'image';
											$link = $isImage ? asset('/storage/project/'.$project->header_image) : 'https://www.youtube.com/watch?v='.$match[1];
											$dataType = $isImage ? 'image' : 'youtube';
										@endphp
										<a href="{{ $link }}" data-fslightbox="{{ $project->class }}" data-type="{{ $dataType }}" data-bs-toggle="lightbox" data-gallery="{{ $project->class }}" onclick="push('Project - {{ $project->project_name }}')">
											<img src="{{ asset('/storage/project/thumb/'.$project->thumb_project) }}" class="project-thumb img-fluid" alt="project-portfolio">
											@if(!$isImage)
											<div class="project-info">
												<div class="project-details video-thumb">
													<i class="fas fa-play-circle fa-3x video-icon"></i>
													<div class="details white-text video-caption">{{ $project->project_name }}</div>
												</div>
											</div>
											@endif
										</a>
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

	<div id="client" class="py-5">
        <section id="focus"> <!-- class="focus3" -->
            <div class="container">
                <div class="section_title">
                    <h1>Clients</h1>
                </div>
                <div id="clients" class="row">
                    @foreach($clients as $client)
                    <div class="col-6 col-sm-4 col-md-3" data-sr="enter left move 200px, hustle 40px, over .2s">
                        <div class="borderBottom">
                            <div class="brandGrid">
                                <div class="borderGrid">
                                    <img src="{{ asset('/storage/clients/'.$client->img_file) }}" class="img-fluid" alt="{{ $client->name }} logo">
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>

	<div id="contact-us" class="py-5">
		<section id="focus" class="py-5"><!-- class="focus4" -->
			<div class="container mb-5">
				<div class="section_title">
					<h1>Contact</h1>
				</div>
				
				<div class="row">
					<div class="col-md-8 mx-auto col-12">
						<div class="mb-5">
							<p class="passage-contact">A little conversation via this form would be the first step from detail conversation, so don’t hesitate to inform what I can do for your project.</p>
						</div>
						<form id="contact-form" method="POST" action="{{ url('/contactus') }}">
							{{ csrf_field() }}
							<div class="row">
								<!-- Name Input -->
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="form-floating mb-3">
										<input id="name" name="name" placeholder="Name" class="form-control" id="floatingInput" type="text" autocomplete="off" required>
										<label for="floatingInput">Name</label>
									</div>
								</div>

								<!-- Phone Input -->
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="form-floating mb-3">
										<input id="phone" type="text" name="phone" class="form-control" id="floatingInput" autocomplete="off" placeholder="Phone Number (WhatsApp number preferrable)" required>
										<label for="floatingInput">Phone Number</label>
									</div>
								</div>

								<!-- Email Input -->
								<div class="col-md-12">
									<div class="form-floating mb-3">
										<input type="email" name="email" class="form-control" id="floatingInput" autocomplete="off" placeholder="name@example.com">
										<label for="floatingInput">Email</label>
									</div>
								</div>
							</div>

							<!-- Message Textarea -->
							<div class="row">
								<div class="col-lg-12">
									<div class="form-floating mb-3">
										<textarea id="message" class="form-control form-control-lg" style="height: 150px;" name="message" placeholder="Write down your message" required></textarea>
										<label for="floatingInput">Your Message</label>
									</div>
								</div>
							</div>

							<!-- Submit Button -->
							<div class="d-flex justify-content-center align-items-center position-relative mb-3">
								<i class="fas fa-circle-notch fa-spin loading"></i>
								<div>
									<div id="finish" class="finish"></div>
									<!-- onclick="push('Submit Email')" -->
									<button class="submit-custom sendEmail" type="submit">
										<span class="fa fa-paper-plane me-2"></span>
										<span class="submit-text">Submit</span>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<div class="container">
				<div class="row">
					<div class="col-md-8 mx-auto">
						<div class="row">
							<div id="footer-location" class="col-12 col-md-6 containerLocation">
								<h3><i style="margin-right: 10px; float:left;" class="fa fa-map-marker fa-2x"></i>Location :<br>Surakarta - Indonesia</h3>
							</div>
							<div id="footer-social" class="col-12 col-md-6 containerSocial ps-md-5">
								<h3>Let's get connected or see my latest work update:</h3>
								<ul>
									@foreach($socials as $social)
									<li class="socialIcon">
										<a href="https://{{ $social->url }}" onclick="push('Social Link - {{ $social->url }}')" target="_blank" class="{{ $social->icon_code }}"></a>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- FOOTER -->
	<div class="container-fluid">
		<div class="row">
			<footer class="col-12 py-4 text-center">
				<p class="logo-footer">
					&middot; &copy; <span style="color:#f8941e;">MM3D</span> <span id="current-year"></span> &middot;
				</p>
			</footer>
		</div>
	</div>
@endsection

@section('js')
<script>
    document.getElementById('current-year').textContent = new Date().getFullYear();
</script>
<script src='{{URL::asset("js/lottie.min.js")}}'></script>
<!-- <script src="{{URL::asset('js/ekko-lightbox.js')}}" type="text/javascript" charset="utf-8"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fslightbox/3.0.9/index.js" type="text/javascript"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js" integrity="sha512-E/yP5UiPXb6VelX+dFLuUD+1IZw/Kz7tMncFTYbtoNSCdRZPFoGN3jZ2p27VUxHEkhbPiLuZhZpVEXxk9wAHCQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<script>
	$(document).ready(function() {
		var $grid = $('.grid').isotope({
			itemSelector: '.grid-item',
			layoutMode: 'fitRows', // Or masonry, depending on your layout needs
			percentPosition: true, // Ensures proper positioning with flex/grid layout
		});

		// Handle filter button clicks
		$('#filter button').click(function() {
			var filterValue = $(this).attr('data-filter');
			$grid.isotope({ filter: filterValue });

			// After filtering, trigger a layout recalculation to fit items neatly
			$grid.isotope('layout');
		});

		// Trigger layout recalculation on window resize to handle layout changes
		$(window).resize(function() {
			$grid.isotope('layout');
		});

		setTimeout(() => {
			$grid.isotope({ filter: '.exterior' });
		}, 1000);
	});




	var animation = bodymovin.loadAnimation({
		container: document.getElementById('finish'),
		renderer: 'svg',
		speed: 1.5,
		loop: false,
		autoplay: false,
		path: '{{URL::asset("img/lf30_editor_P7N6G6.json")}}'
	});
	
	let isPost = 0;

    const showLoading = () => {
        $(".loading").addClass("reveal");
        $(".submit-text").addClass("text-vanish");
        $(".submit-custom").css("opacity", "0");
    };

    const hideLoading = () => {
        $(".loading").removeClass("reveal");
        $(".submit-text").removeClass("text-vanish");
        $(".submit-custom").css("opacity", "1");
    };

    const showToast = (icon, title) => {
        Swal.fire({
            icon: icon,
            title: title,
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
        });
    };

    const resetForm = () => {
		clearErrors();
        $('#contact-form')[0].reset();
		hideLoading();
    };

    const handleErrors = (xhr) => {
		clearErrors();
        $('.form-control').removeClass('is-invalid');
        $('.invalid-feedback').remove();

        if (xhr.responseJSON && xhr.responseJSON.errors) {
            let errors = xhr.responseJSON.errors;

            for (let field in errors) {
                let errorMessages = errors[field].join(', ');

                let input = $(`[name="${field}"]`);
                input.addClass('is-invalid');
                input.after(`<div class="invalid-feedback">${errorMessages}</div>`);
            }
        }
    };

	const clearErrors = () => {
		$('.form-control').removeClass('is-invalid');
		$('.invalid-feedback').remove();
	};

    $('.sendEmail').click(function(e) {
        e.preventDefault();
		clearErrors();

        if (isPost != 0) {
			showToast('warning', 'Wait! Send email is in process...');
            return;
        }

        showLoading();
        isPost = 1;

        const data = $('#contact-form').serialize(); // Serialize form data

        $.ajax({
            method: "GET",
            url: "{{ url('/contactus') }}",
            data: data,
            success: function(response) {
                $(".finish").addClass("reveal");

                setTimeout(() => lottie.play(), 100);
                setTimeout(() => {
                    showToast('success', 'Your message has been sent. Thank you!');
					hideLoading();
                    resetForm();
                    $(".finish").removeClass("reveal");
                    isPost = 0;
                    animation.goToAndStop(0, true);
                }, 3000);
            },
            error: function(xhr) {
                isPost = 0;
                hideLoading();
                handleErrors(xhr);
                showToast('error', 'Failed!');
            }
        });
    });

	// Check Phone Number
	var inputEl = document.querySelector('#phone');
	var goodKey = '0123456789+ ';
	// console.log(inputEl);

	var checkInputTel = function(e) {
		var key = (typeof e.which == "number") ? e.which : e.keyCode;
		var start = this.selectionStart,
			end = this.selectionEnd;

		var filtered = this.value.split('').filter(filterInput);
		this.value = filtered.join("");

		/* Prevents moving the pointer for a bad character */
		var move = (filterInput(String.fromCharCode(key)) || (key == 0 || key == 8)) ? 0 : 1;
		this.setSelectionRange(start - move, end - move);
	}

	var filterInput = function(val) {
		return (goodKey.indexOf(val) > -1);
	}

	inputEl.addEventListener('input', checkInputTel);

	function push(event) {
		gtag('event', 'Click',{ 
			'event_category': event,
			'event_action': 'click',
			'value': 1
		});
	}
</script>
@endsection