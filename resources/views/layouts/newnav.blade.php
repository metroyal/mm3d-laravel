<nav class="navbar fixed-top navbar-light bg-light navbar-expand-lg">
    <div class="container w-100">
        <!-- Logo aligned to the left -->
        <a class="navbar-brand" href="#">
            <img src="{{ asset('img/logo_orange.png') }}" height="50" alt="mm3darsitektur logo">
        </a>
        <!-- Toggler for mobile view -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Navbar items aligned to the right -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0 proxima_novalight">
                <li class="nav-item me-lg-3 text-center">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item me-lg-3 text-center">
                    <a class="nav-link" href="#service">Service</a>
                </li>
                <li class="nav-item me-lg-3 text-center">
                    <a class="nav-link" href="#works">Works</a>
                </li>
                <li class="nav-item me-lg-3 text-center">
                    <a class="nav-link" href="#client">Client</a>
                </li>
                <li class="nav-item text-center">
                    <a class="nav-link" href="#contact-us">Contact Us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
