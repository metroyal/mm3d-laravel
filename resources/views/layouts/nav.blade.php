<div class="logo">
    <a class="link" rel="home" href="{{ (request()->is('/')) ? '#home' : '/' }}" onclick="push('NavBar - Logo')">
        <img src="{{ asset('img/logo_orange.png') }}" width="307" alt="mm3darsitektur logo">
    </a>
</div>
<div id="top" class="navigation-wrap">
    <nav class="nav-collapse">
        <ul class="navbar-nav">
            <li><a href="{{ (request()->is('/')) ? '#home' : '/' }}" onclick="push('NavBar - Home')">Home</a></li>
            <li><a href="{{ (request()->is('/')) ? '#focus' : '/#focus' }}" onclick="push('NavBar - About')">About</a></li>
            <li><a href="{{ (request()->is('/')) ? '#parallax2' : '/#parallax2' }}" onclick="push('NavBar - Works')">Works</a></li>
            <li><a href="{{ (request()->is('/')) ? '#client' : '/#client' }}" onclick="push('NavBar - Client')">Clients</a></li>
            <li><a href="{{ (request()->is('/')) ? '#contact-us' : '/#contact-us' }}" onclick="push('NavBar - Contact')">Contact</a></li>
        </ul>
    </nav>
</div>