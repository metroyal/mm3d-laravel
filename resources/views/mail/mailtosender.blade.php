<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Your Email is Sent to mm3darsitektur.com</title>
</head>
<body>
    <h2>Terima Kasih {!! $content['name'] !!}, Telah Menghubungi MM3D Arsitektur</h2>
    </br>
    <p>Isi pesan anda adalah:</p>
    </br>
    <p>{!! $content['message'] !!}</p>
    </br>
    </br>
    <p>Kami akan membalas pesan anda dalam waktu maksimal 48 jam.</p>
</body>
</html>