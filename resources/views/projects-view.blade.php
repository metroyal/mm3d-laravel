@extends('template')

@section('title')
    MM3D - Project
@stop

@section('css')
<link rel="stylesheet" href="{{URL::asset('css/style.css')}}" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="{{URL::asset('css/projectDetail.css')}}" type="text/css" media="screen" charset="utf-8">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet" />
<link rel="stylesheet" href="{{URL::asset('css/ekko-lightbox.css')}}" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="https://cdn.pannellum.org/2.4/pannellum.css"/>
@endsection

@section('content')
<div class="main-header darken-menu">
    @include('layouts.nav')
</div>
<div id="home" class="navbar-wrapper headerProject" style="background-image: url(../img/bg_projects.jpg)"></div>
<div class="header">
    <div class="vertical-center caption">
        <h1 data-sr="enter top move 200px, over 1s, wait 0.5s">Projects</h1>
        <div class="appscreen" data-sr="move bottom move 300px, vFactor 0.4, over 1s, wait 1s">
            <!-- <img src="" width="1125" height="606" alt="AppScreen"> -->
        </div>
    </div>
</div>

<!-- Intro & Role
================================================== -->

<section class="main-container">
    <div class="container">
        <div class="row no-padding">
            <div id="categories-menu" class="section_title mt-50">
                <h1>Categories</h1>
                <span>
                    <a {{ Request::is('projects') ? 'href=# class=active' : 'href=/projects' }}>
                        <span {{ Request::is('projects') ? 'class=active' : '' }}>All</span>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-50">
        <div class="row no-padding">
            <div id="content" class="col-md-12 no-padding">
            </div>
        </div>
    </div>
</section>

<div class="container">
    <footer class="col-md-12 mb-20">
        <p class="logo-footer">
            &middot; &copy; <font color="#f8941e">MM 3D</font>
            <script>
                document.querySelector('.logo-footer').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>
            &middot;
        </p>
    </footer>
</div>
@endsection

@section('js')
<script src="{{URL::asset('js/ekko-lightbox.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            return $(this).ekkoLightbox({});
        });
    });

    var categories = '';
    $.ajax({
        url: '/api/categories/',
        type: 'GET',
        success: function(result) {
            if(window.location.pathname == '/projects'){
                loadCategories();
            }
            $.each(result.data, function(key, data) {
                categories += "<span>";
                if(window.location.pathname == '/projects/'+data.class){
                    categories += "<a href=# class=active><span class=active>"+data.category_name+"</span></a></span>";
                    loadCategoryByClass(data.class);
                } else {
                    categories += "<a href='/projects/"+data.class+"'><span>"+data.category_name+"</span></a></span>";
                };
            });
        },
    }).done(result => {
        $('#categories-menu').append(categories);
    });

    function loadCategories() {
        var projects = '';
        $.ajax({
            url: '/api/projects',
            type: 'GET',
            success: function(result) {
                console.log(result);
                $.each(result.data, function(key, data) {
                    if(data.category_type == 'video') {
                        videoUrl = data.youtube_link;
                        var findUrl = /src="([^"]+)"/;
                        var match = findUrl.exec(videoUrl);
                        var url = match[1];
                        
                        var videoIdRegex = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*?[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
                        var matchId = url.replace(videoIdRegex, 'http://www.youtube.com/watch?v=$1');
                        
                        projects += `
                            <figure class="col-lg-3 col-md-4 col-sm-6" style="padding: 5px;">
                                <a href="`+matchId+`" data-toggle="lightbox" data-gallery="mm3d" data-type="youtube">
                                    <img alt="picture" src="{{ '/storage/project/thumb/`+data.thumb_project+`' }}" class="img-fluid" style="width: 100%;">
                                    <div class="project-info">
                                        <div class="project-details video-thumb">
                                            <h5 class="play-button vertical-center m-0"><i class="fas fa-play-circle fa-4x"></i></h5>
                                            <div class="details white-text video-caption"> `+data.project_name+` </div>
                                        </div>
                                    </div>
                                </a>
                            </figure>
                        `;
                    } else {
                        projects += `
                            <figure class="col-lg-3 col-md-4 col-sm-6" style="padding: 5px;">
                                <a href="{{ '/storage/project/`+data.header_image+`' }}" data-toggle="lightbox" data-gallery="mm3d" data-type="image">
                                    <img alt="picture" src="{{ '/storage/project/thumb/`+data.thumb_project+`' }}" class="img-fluid" style="width: 100%;">
                                    <div class="project-info">
                                        <div class="project-details video-thumb">
                                            <h5 class="play-button vertical-center m-0"><i class="fas fa-camera-retro fa-4x"></i></h5>
                                        </div>
                                    </div>
                                </a>
                            </figure>
                        `;
                    }
                });
            },
        }).done(result => {
            $('#content').append(projects);
        });
    };

    function loadCategoryByClass(className) {
        var projects = '';
        $.ajax({
            url: '/api/project/'+className,
            type: 'GET',
            onError: function(xhr, status, error){
                alert("error: " + error);
                console.log(xhr.responseJSON.messages)
            },
            success: function(result) {
                console.log(result);
                if(result.status == 200) {
                    $.each(result.data, function(key, data) {
                        if(data.category_type == 'video') {
                            videoUrl = data.youtube_link;
                            var findUrl = /src="([^"]+)"/;
                            var match = findUrl.exec(videoUrl);
                            var url = match[1];
                            
                            var videoIdRegex = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*?[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
                            var matchId = url.replace(videoIdRegex, 'http://www.youtube.com/watch?v=$1');
                            
                            projects += `
                                <figure class="col-lg-3 col-md-4 col-sm-6" style="padding: 5px;">
                                    <a href="`+matchId+`" data-toggle="lightbox" data-gallery="mm3d" data-type="youtube">
                                        <img alt="picture" src="{{ '/storage/project/thumb/`+data.thumb_project+`' }}" class="img-fluid" style="width: 100%;">
                                        <div class="project-info">
                                            <div class="project-details video-thumb">
                                                <h5 class="play-button vertical-center m-0"><i class="fas fa-play-circle fa-4x"></i></h5>
                                                <div class="details white-text video-caption"> `+data.project_name+` </div>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                            `;
                        } else {
                            projects += `
                                <figure class="col-lg-3 col-md-4 col-sm-6" style="padding: 5px;">
                                    <a href="{{ '/storage/project/`+data.header_image+`' }}" data-toggle="lightbox" data-gallery="mm3d" data-type="image">
                                        <img alt="picture" src="{{ '/storage/project/thumb/`+data.thumb_project+`' }}" class="img-fluid" style="width: 100%;">
                                        <div class="project-info">
                                            <div class="project-details video-thumb">
                                                <h5 class="play-button vertical-center m-0"><i class="fas fa-camera-retro fa-4x"></i></h5>
                                            </div>
                                        </div>
                                    </a>
                                </figure>
                            `;
                        }
                    });
                } else {
                    projects += `
                                <div class="col-sm-12 caption" style="padding: 5px;">
                                    <h1>Data Not Found</h1>
                                </div>
                            `;
                }
                
            },
        }).done(result => {
            $('#content').append(projects);
        });
    };
</script>
@endsection