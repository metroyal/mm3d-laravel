<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>@yield('title')</title>
        <!-- Favicon-->
        <link rel="icon" href="{{ URL::asset('cmsBSB/favicon.ico') }}" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <link href="{{ URL::asset('/css/app.css') }}" rel="stylesheet">

        <!-- Bootstrap Core Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/node-waves/waves.css') }}" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/animate-css/animate.css') }}" rel="stylesheet" />

        <!-- Dropzone Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

        <!-- Bootstrap DatePicker Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />

        <!-- Wait Me Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/waitme/waitMe.css') }}" rel="stylesheet" />

        <!-- Morris Chart Css-->
        <link href="{{ URL::asset('cmsBSB/plugins/morrisjs/morris.css') }}" rel="stylesheet" />

        <!-- Bootstrap Select Css -->
        <link href="{{ URL::asset('cmsBSB/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="{{ URL::asset('cmsBSB/css/style.css') }}" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="{{ URL::asset('cmsBSB/css/themes/theme-teal.css') }}" rel="stylesheet" />

        <!-- Notification CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">
        @yield('extraCSS')
    </head>
    <body class="theme-teal">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->

        @include('admin.layout.header')

        @include('admin.layout.sidebar')
        
        @yield('body')

        <!-- Jquery Core Js -->
        <!-- <script src="{{ URL::asset('cmsBSB/plugins/jquery/jquery.min.js') }}"></script> -->
        <script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>

        <!-- Bootstrap Core Js -->
        <!-- <script src="{{ URL::asset('cmsBSB/plugins/bootstrap/js/bootstrap.js') }}"></script> -->
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Notification JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.js"></script>

        <!-- Select Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

        <!-- Dropzone Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/dropzone/dropzone.js') }}"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/node-waves/waves.js') }}"></script>

        <!-- Autosize Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/autosize/autosize.js') }}"></script>

        <!-- Moment Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/momentjs/moment.js') }}"></script>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

        <!-- Bootstrap Datepicker Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

        <!-- Jquery CountTo Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/jquery-countto/jquery.countTo.js') }}"></script>

        <!-- Morris Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/raphael/raphael.min.js') }}"></script>
        <script src="{{ URL::asset('cmsBSB/plugins/morrisjs/morris.js') }}"></script>

        <!-- ChartJs -->
        <script src="{{ URL::asset('cmsBSB/plugins/chartjs/Chart.bundle.js') }}"></script>

        <!-- Flot Charts Plugin Js -->
        <!-- <script src="{{ URL::asset('cmsBSB/plugins/flot-charts/jquery.flot.resize.js') }}"></script>
        <script src="{{ URL::asset('cmsBSB/plugins/flot-charts/jquery.flot.pie.js') }}"></script>
        <script src="{{ URL::asset('cmsBSB/plugins/flot-charts/jquery.flot.categories.js') }}"></script>
        <script src="{{ URL::asset('cmsBSB/plugins/flot-charts/jquery.flot.js') }}"></script>
        <script src="{{ URL::asset('cmsBSB/plugins/flot-charts/jquery.flot.time.js') }}"></script> -->

        <!-- Sparkline Chart Plugin Js -->
        <script src="{{ URL::asset('cmsBSB/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

        <!-- Custom Js -->
        <!-- <script src="{{ URL::asset('cmsBSB/js/pages/index.js') }}"></script> -->
        <script src="{{ URL::asset('cmsBSB/js/pages/forms/basic-form-elements.js') }}"></script>
        <script src="{{ URL::asset('cmsBSB/js/admin.js') }}"></script>
        

        @yield('script')
        <!-- Demo Js -->
        <script src="{{ URL::asset('cmsBSB/js/demo.js') }}"></script>
        @include('admin.dashboard.notification')
    </body>
</html>
