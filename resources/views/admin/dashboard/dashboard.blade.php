@extends('admin.layout.template')

@section('title')
    Admin Dashboard
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>
        <div class="row cardcontainer">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box-3 bg-teal hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">card_travel</i>
                    </div>
                    <div class="content">
                        <div class="text">NUMBER OF PROJECTS</div>
                        <div class="number count-to" data-from="0" data-to="25" data-speed="1000" data-fresh-interval="20">{{$counttotalproject}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box-3 bg-blue hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">perm_media</i>
                    </div>
                    <div class="content">
                        <div class="text">IMAGE ON CAROUSEL</div>
                        <div class="number count-to" data-from="0" data-to="7" data-speed="1000" data-fresh-interval="20">{{$counttotalcarousel}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box-3 bg-green hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">access_alarm</i>
                    </div>
                    <div class="content">
                        <div class="text">TIME NOW</div>
                        <div class="number">{{ $mytime->format('g:i A') }}</div>
                    </div>
                </div>
            </div>
            <a href="/logout" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 buttonlogout">
                <div class="info-box-3 bg-deep-orange hover-expand-effect logout">
                    <div class="icon">
                        <i class="material-icons">power_settings_new</i>
                    </div>
                    <div class="content">
                        <div class="text">Admin</div>
                        <div class="number">LOG OUT</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- Striped Rows -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            MM3D - PROJECT LIST
                            <small>Daftar project yang ada dalam database</small>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Project Name</th>
                                    <th>Date Added</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($projects as $key => $project)
                                <tr>
                                    <th scope="row">{{ $key + 1 }}</th>
                                    <td>{{ $project->project_name }}</td>
                                    <td>{{ date('d-M-Y', strtotime($project->created_at->toDateString())) }} | {{ $project->created_at->format('g:i A') }}</td>
                                    <td>@if($project->is_active == 1) Show @elseif($project->is_active == 0) Hide @endif</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Striped Rows -->
    </div>
</section>
@stop