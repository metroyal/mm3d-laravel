@extends('admin.layout.template')

@section('title')
    Client Testimonial
@stop

@section('extraCSS')
<link href="{{ URL::asset('cmsBSB/plugins/sweetalert/sweetalert2.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('cmsBSB/plugins/ion-rangeslider/css/normalize.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('cmsBSB/plugins/font-awesome-iconpicker/css/fontawesome-iconpicker.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet" />
@stop

@section('body')
<section class="content js-sweetalert">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Settings
                            <small>Change your Social Link and profile</a></small>
                        </h2>
                        <!-- <button class="btn btn-default action-create">Create instances</button> -->
                        <ul class="header-dropdown putOnRight m-r--5">
                            <!-- <div class="col-xs-3"> -->
                                <a type="button" class="btn btn-block btn-lg btn-primary waves-effect" data-toggle="modal" data-target="#defaultModal"><i class="material-icons">add_circle</i><span class="showHide">ADD SOCIAL LINK</span></a>
                            <!-- </div> -->
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Icon</th>
                                    <th class="text-center">URL</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class='sort-list'>
                                <?php $i=1; ?>
                                @foreach ($getDataContacts as $getDataContact)
                                <tr class="dataArray" data-index="{{ $getDataContact->id }}" data-position="{{ $getDataContact->order_number }}">
                                    <th scope="row">
                                        <?php echo $i; ?>.
                                    </th>
                                    <td>
                                        <i class="{{ $getDataContact->icon_code }}" style="font-size: 37px !important;"></i>
                                    </td>
                                    <td class="text-center">
                                        <p class="">https://{{ $getDataContact->url }}</p>
                                    </td>
                                    <td class="text-center">
                                        @if ($getDataContact->is_active == 1)
                                        <span class="label bg-green">Active</span>
                                        @else
                                        <span class="label bg-deep-orange">Inactive</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="#" type="button" data-toggle="modal" data-target="#editTesti" data-edit="{{$getDataContact->id}}" class="btn btn-primary waves-effect" onclick="edit({{$getDataContact->id}})"> <!-- REVIEW  -->
                                            <i class="material-icons">edit</i>
                                            <span>EDIT</span>
                                        </a>
                                        @if($getDataContact->is_active == 0) 
                                        <button type="button" class="btn btn-success waves-effect" onClick="setActiveStatus('{{$getDataContact->id}}')">
                                            <i class="material-icons">visibility</i>
                                            <span>Show</span>
                                        </button>
                                        @else
                                        <button type="button" class="btn btn-warning waves-effect" onClick="setActiveStatus('{{$getDataContact->id}}')">
                                            <i class="material-icons">visibility_off</i>
                                            <span>Hide</span>
                                        </button>
                                        @endif
                                        <button type="button" class="btn btn-danger waves-effect" data-value="{{$getDataContact->id}}" data-type="confirm">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="POST" action="{{ url('/admin/storeProfile') }}">
                <div class="modal-content">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Add New Social Link</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <label class="form-label">Choose Your Icon</label>
                            <input data-placement="rightBottom" class="form-control icp icp-auto" name="iconCode" style="display: none !important;" value="fas fa-icons" type="text"/>
                            <span class="input-group-addon" style="width: 60px !important; height: 60px !important; font-size: 30px !important;"></span>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="url" value="" class="form-control">
                                <label class="form-label">URL</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect" data-type="saveContact">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="editTesti" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <!-- <form method="POST"> -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Edit Social Link</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <label class="form-label">Current Icon</label>
                            <span class="input-group-addon inputAddonEdit" style="border: 1px solid #ccc !important; border-radius: 3px !important ;width: 60px !important; height: 60px !important; font-size: 30px !important;"></span>
                        </div>
                        <div class="form-group form-float">
                            <label class="form-label">Choose New Icon</label>
                            <input data-placement="bottomRight" class="form-control icp icp-auto" name="iconCode_edit" style="display: none !important;" value="" type="text"/>
                            <input name="iconCode_edit1" value="" type="hidden"/>
                            <span class="input-group-addon" style="width: 60px !important; height: 60px !important; font-size: 30px !important;"></span>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line editForm">
                                <input type="text" name="urlEdit" value="" class="form-control">
                                <label class="form-label">URL</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect" data-type="saveTesti" onclick="storeEdit()">UPDATE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" onclick="removeElement()">CANCEL</button>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.0/dist/sweetalert2.all.min.js"></script>
<script src="{{ URL::asset('cmsBSB/plugins/font-awesome-iconpicker/js/fontawesome-iconpicker.min.js') }}"></script>
<script src="{{ URL::asset('cmsBSB/plugins/font-awesome-iconpicker/js/iconpicker-trigger.js') }}"></script>
<script>
    var idContact;

    function removeElement() {
        $('.removeThis').remove();
    }

    $('.testiContent').text(function(index, currentText) {
        if(currentText.length > 25){
            return currentText.substr(0, 25,) + '...';
        }
    });

    // $('.editTesti').on('click', function() {
    //     $(this).children().each(function (index) {
    //         // var editId = $(this).data('edit').val();
    //         // console.log('editId:', editId);
    //     )}
    // });

    function edit(id) { // TODO  
        $('.editForm').addClass('focused');
        window.idContact = id;
        $.ajax({
            url: "{{ url('/admin/editcontact/') }}" + '/' + id,
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                console.log(response.icon);
                $('input[name="iconCode_edit1"]').val(response.icon);
                $('input[name="iconCode_edit"]').val(response.class);
                $('.inputAddonEdit').append(response.icon);
                $('input[name="urlEdit"]').val(response.url);
            }
        });
    }

    function storeEdit() { // TODO
        id = idContact;
        token = $("input[name='_token']").val();
        iconCode_edit = $("input[name='iconCode_edit']").val();
        urlEdit = $("input[name='urlEdit']").val();
        $.ajax({
            url: "{{ url('/admin/storeeditcontact/') }}" + '/' + id,
            method: 'GET',
            dataType: 'json',
            data: {
                token:token,
                iconCode_edit:iconCode_edit,
                urlEdit:urlEdit,
            },
            success: function (response) {
                if(response.status == 'ok'){
                    location.reload();
                }
            }
        });
    }

    $('.js-sweetalert button').on('click', function () {
        var type = $(this).data('type');
        var id = $(this).data('value');
        var token = $('input[name="_token"]').val();
        if (type === 'confirm') {
            showConfirmation(id,token);
        }
    });

    function showConfirmation(id, token) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "/api/contact/delete/" + id,
                    method: "delete",
                    data:{
                        id: id,
                        token: token
                    },
                    success: function(data) {
                        if(data.code == 200) {
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 500);
                        }
                    },
                    error: function(data) {
                        Swal.fire({
                            title: 'Error!',
                            text: data,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                })
            };
        })
    }

    function setActiveStatus(id) {
        var token = $('input[name="_token"]').val();
        
        $.ajax({
            url: "/api/contact/setActive/" + id,
            method: 'POST',
            dataType: 'json',
            data: {
                token:token,
            },
            success: function (response) {
                if(response.code == 200){
                    Swal.fire({
                        title: 'Success!',
                        text: response.message,
                        icon: 'success',
                        confirmButtonText: 'Ok'
                    });

                    setTimeout(() => {
                        location.reload();
                    }, 500);
                }
            },
            error: function(data) {
                Swal.fire({
                    title: 'Error!',
                    text: data,
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            }
        });
    }

</script>
@stop