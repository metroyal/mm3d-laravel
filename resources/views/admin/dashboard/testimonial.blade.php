@extends('admin.layout.template')

@section('title')
    Client Testimonial
@stop

@section('extraCSS')
<link href="{{ URL::asset('cmsBSB/plugins/sweetalert/sweetalert2.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('cmsBSB/plugins/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('cmsBSB/plugins/ion-rangeslider/css/ion.rangeSlider.skinNice.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('cmsBSB/plugins/ion-rangeslider/css/normalize.css') }}" rel="stylesheet" />
@stop

@section('body')
<section class="content js-sweetalert">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Client Testimonial
                            <small>All of your Clients Testimonial, from the past projects</a></small>
                        </h2>
                        <ul class="header-dropdown putOnRight m-r--5">
                            <!-- <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li> -->
                            <!-- <div class="col-xs-3"> -->
                                <a type="button" class="btn btn-block btn-lg btn-primary waves-effect" data-toggle="modal" data-target="#defaultModal"><i class="material-icons">add_circle</i><span class="showHide">ADD TESTIMONIAL</span></a>
                            <!-- </div> -->
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Client Name</th>
                                    <th>Testimony</th>
                                    <th class="text-center">Rating</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Order Number</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class='sort-list'>
                                <?php $i=1; ?>
                                
                                @foreach ($testimonials as $testimonial)
                                
                                <tr class="dataArray" data-index="{{ $testimonial->id }}" data-position="{{ $testimonial->order_number }}">
                                    <th scope="row">
                                        <?php echo $i; ?>.
                                    </th>
                                    <td>
                                        <p class="font-bold">{{ $testimonial->name }}</p>
                                    </td>
                                    <td>
                                        <p class="testiContent">{{ $testimonial->testimonial }}</p>
                                    </td>
                                    <td class="text-center">
                                        <span class="label bg-light-blue">{{ $testimonial->rate }} Stars</span>
                                    </td>
                                    <td class="text-center">
                                        @if ($testimonial->is_active == 1)
                                        <span class="label bg-green">Active</span>
                                        @else
                                        <span class="label bg-deep-orange">Inactive</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{ $testimonial->order_number }}
                                        <input class='originalposition' type='hidden' name='item[origposition]' value='{{ $testimonial->order_number }}'>
                                        <input class='currentposition' type='hidden' name='currposition' value='{{ $testimonial->order_number }}'>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" type="button" data-toggle="modal" data-target="#editTesti" data-edit="{{$testimonial->id}}" class="btn btn-primary waves-effect" onclick="edit({{$testimonial->id}})"> <!-- REVIEW  -->
                                            <i class="material-icons">edit</i>
                                            <span>EDIT</span>
                                        </a>
                                        <button type="button" class="btn btn-danger waves-effect" data-value="{{$testimonial->id}}" data-type="confirm">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="POST" action="{{ url('/admin/storeTestimonial') }}">
                <div class="modal-content">
                {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Add New Testimonial</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="clientname" value="{{ old('projectname') }}" class="form-control">
                                <label class="form-label">Client Name</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="testimonial" value="{{ old('projectname') }}" class="form-control">
                                <label class="form-label">Testimonial</label>
                            </div>
                        </div>
                        <div class="irs-demo">
                            <b>Rating</b>
                            <input type="text" name="range_testi" id="range_testi" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect" data-type="saveTesti">SAVE TESTIMONIAL</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="editTesti" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <!-- <form method="POST"> -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Edit Testimonial</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <div class="form-line editForm">
                                <input type="text" name="clientnameEdit" value="" class="form-control">
                                <label class="form-label">Client Name</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line editForm">
                                <input type="text" name="testimonialEdit" value="" class="form-control">
                                <label class="form-label">Testimonial</label>
                            </div>
                        </div>
                        <div class="irs-demo">
                            <b>Rating</b>
                            <input type="text" name="range_testiEdit" id="edit_testi" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect" data-type="saveTesti" onclick="storeEdit()">SAVE TESTIMONIAL</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.0/dist/sweetalert2.all.min.js"></script>
<script src="{{ URL::asset('cmsBSB/plugins/ion-rangeslider/js/ion.rangeSlider.js') }}"></script>
<script src="{{ URL::asset('cmsBSB/js/pages/ui/range-sliders.js') }}"></script>
<script>
    var idTesti;

    $('.testiContent').text(function(index, currentText) {
        if(currentText.length > 25){
            return currentText.substr(0, 25,) + '...';
        }
    });

    // $('.editTesti').on('click', function() {
    //     $(this).children().each(function (index) {
    //         // var editId = $(this).data('edit').val();
    //         // console.log('editId:', editId);
    //     )}
    // });

    function edit(id) { // TODO  
        $('.editForm').addClass('focused');
        window.idTesti = id;
        $.ajax({
            url: "{{ url('/admin/edittestimonial/') }}" + '/' + id,
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                console.log(response);
                $('input[name="clientnameEdit"]').val(response.name);
                $('input[name="testimonialEdit"]').val(response.testi);

                var instance = $('input[name="range_testiEdit"]').data("ionRangeSlider");

                instance.update({
                    from: response.rate
                });
            }
        });
    }

    function storeEdit() { // TODO
        id = idTesti;
        token = $("input[name='_token']").val();
        clientnameEdit = $("input[name='clientnameEdit']").val();
        testimonialEdit = $("input[name='testimonialEdit']").val();
        range_testiEdit = $("input[name='range_testiEdit']").val();
        $.ajax({
            url: "{{ url('/admin/updatetesti/') }}" + '/' + id,
            method: 'GET',
            dataType: 'json',
            data: {
                token:token,
                clientnameEdit:clientnameEdit,
                testimonialEdit:testimonialEdit,
                range_testiEdit:range_testiEdit,
            },
            success: function (response) {
                if(response.status == 'ok'){
                    location.reload();
                }
            }
        });
    }

    $('.sort-list').sortable({
        axis: 'y',
        opacity: 0.7,
        cursor: 'pointer',
        update: function (event, ui) {
            $(this).children().each(function (index) {
                if ($(this).attr('data-position') != (index+1)) {
                    $(this).attr('data-position', (index+1)).addClass('updated');
                }
            });
            saveNewPositions();
        }
    });
    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function () {
            positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
            $(this).removeClass('updated');
        });
        $.ajax({
            url: "{{ route('/admin/setTestiOrder') }}",
            method: 'GET',
            dataType: 'text',
            data: {
                update: 1,
                positions: positions
            }, success: function (response) {
                location.reload();
            }
        });
    };

    $('.js-sweetalert button').on('click', function () {
        var type = $(this).data('type');
        var id = $(this).data('value');
        var token = $('input[name="_token"]').val();
        if (type === 'confirm') {
            showConfirmation(id,token);
            // console.log('id:', id, ' Token:', token);
        }
    });

    function showConfirmation(id,token) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success',
            )
            setTimeout(function() {
                $.ajax({
                    url:"{{ route('/admin/deleteTesti') }}",
                    method:"GET",
                    data:{id:id,token:token},
                    success: function(data) {
                        location.reload();
                    }
                })}, 1000)
            };
        })
    }

    // $('button[data-type="saveTesti"]').click(function(){
    //     token = $("input[name='_token']").val();
    //     name = $("[name='clientname']").val();
	// 	testi = $("[name='testimonial']").val();
    //     rating = $("[name='range_testi']").val();
    //     console.log(token + ' ' + testi + ' ' + rating);
    //     $.ajax({
    //         method:"POST",
    //         data:{token:token,clientname:name,testimonial:testi,rate:rating},
    //         url: "{{ url('/admin/storeTestimonial') }}",
    //         success: function (response) {
    //             console.log(response);
    //             // location.reload();
    //         }
    //     });
    // });
</script>
@stop