@extends('admin.layout.template')

@section('title')
    Add New Project
@stop

@section('extraCSS')
<!-- <link href="{{ URL::asset('cmsBSB/plugins/fileuploaderjs/css/custom.css') }}" rel="stylesheet"/> -->

@endsection

@section('body')
<section class="content">
    <div class="container-fluid">
        <!-- Horizontal Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ADD PROJECT
                        </h2>
                    </div>
                    <div class="body">
                    <!-- @if(session()->has('message'))
                    <div class="alert alert-success alertSubmit animated">
                        {{ session()->get('message') }}
                    </div>
                    @endif -->
                        <form method="post" action="{{ url('admin/storeproject') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                            <div>
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <!-- @if($errors->first('projectname'))
                                            <div class="invalid-feedback">
                                            {{ $errors->first('projectname') }}
                                            </div>
                                        @endif -->
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="projectname" value="{{ old('projectname') }}" class="form-control">
                                                <label class="form-label">Project Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="display: none">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="clientname" value="{{ old('clientname') }}" class="form-control">
                                                <label class="form-label">Client Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3" style="display: none">
                                        <div class="form-group form-float">
                                            <div class="form-line" id="bs_datepicker_container">
                                                <input type="text" name="datestart" value="{{ old('datestart') }}"  class="form-control" data-date-format="dd/mm/yyyy">
                                                <label class="form-label">Date of Project Start</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="category" class="form-control show-tick">
                                            <option value="">-- Select Project Category --</option>
                                            @foreach ($categories as $category)
                                            <option value="{{ $category->id_category }}" data-type="{{ $category->category_type }}" {{ (old("category") == $category->id_category ? "selected":"") }}>{{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="categorytype">
                                    </div>
                                    <div id="videoUpload" class="col-sm-12" style="display: none">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <h2 class="card-inside-title">YouTube Embed</h2>
                                                <textarea name="youtubeLink" id="editor" rows="10" cols="80">
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="display: none">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <textarea name="description" rows="1" class="form-control no-resize auto-growth">{{ old('description') }}</textarea>
                                                <label class="form-label">Project Description</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div id="imageUpload" class="col-sm-6" style="display: none">
                                        <div class="form-group form-float">
                                            <h2 class="card-inside-title">Project Image</h2>
                                            <input type="file" name="thumbProject" value="{{ old('thumbProject') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="display: none">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="projLocation" value="{{ old('projLocation') }}" class="form-control">
                                                <label class="form-label">Project Location</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group input-group-lg">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" class="filled-in" name="use360" id="ig_checkbox" value="1">
                                                    <label for="ig_checkbox"></label>
                                                </span>
                                                <span type="text" class="form-control">Use 360 Virtual Tour</span>
                                            </div>
                                        </div>
                                        <div id="input360" style="display: none" class="form-group form-float">
                                            <h2 class="card-inside-title">Upload 360 File</h2>
                                            <input type="file" name="360file" value="{{ old('file_name') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-12 noMargin text-center">
                                    <button type="submit" class="btn bg-primary waves-effect process">
                                        <i class="material-icons">save</i>
                                        <span>SAVE PROJECT</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Horizontal Layout -->
    </div>
</div>
@endsection

@section('script')
<script src="{{ URL::asset('cmsBSB/plugins/ckeditor4/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'editor' );

    $('[name="category"]').on('change', function() {
        var type = $(this.options[this.selectedIndex]).attr('data-type');
        if(type == 'image') {
            $('#imageUpload').slideDown();
            $('#videoUpload').slideUp();
        } else if(type == 'video') {
            $('#imageUpload').slideDown();
            $('#videoUpload').slideDown();
        }
        $('[name="categorytype"]').val(type);
    })

    $(document).ready(function(){
        $('#ig_checkbox').change(function(){
            if (this.checked) {
                $('#input360').slideDown(200, 'swing');
            }
            else {
                $('#input360').slideUp(200, 'swing');
            }                   
        });
    });
</script>

@endsection