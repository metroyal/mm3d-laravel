@extends('admin.layout.template')

@section('title')
    Project Manager
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-12 col-sm-6">
                    <h3>PROJECT LIST</h3>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    
                </div>
            </div>
        </div>
        <!-- Striped Rows -->
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box-3 bg-green hover-zoom-effect waves-effect">
                    <div class="icon">
                        <i class="material-icons">check_circle</i>
                    </div>
                    <div class="content">
                        <div class="text">Shows On Web</div>
                        <div class="number count-to" data-from="0" data-to="{{ $countactiveproject }}" data-speed="2000" data-fresh-interval="20">{{ $countactiveproject }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box-3 bg-deep-orange hover-zoom-effect waves-effect">
                    <div class="icon">
                        <i class="material-icons">remove_circle</i>
                    </div>
                    <div class="content">
                        <div class="text">Hidden Project</div>
                        <div class="number count-to" data-from="0" data-to="{{ $countinactiveproject }}" data-speed="2000" data-fresh-interval="20">{{ $countinactiveproject }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            MM3D - PROJECT LIST
                            <small>Daftar project yang ada dalam database</small>
                        </h2>
                        <ul class="header-dropdown putOnRight m-r--5">
                            <a type="button" href="newproject" class="btn btn-block btn-lg btn-primary waves-effect"><i class="material-icons">add_circle</i><span>ADD PROJECT</span></a>
                        </ul>
                    </div>
                    <div class="body">
                        {{ csrf_field() }}
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            @foreach ($categories as $keys => $category)
                            <li role="presentation" @if($keys == 0) class="active" @endif><a href="#{{$category->id_category}}" data-toggle="tab">{{$category->category_name}}</a></li>
                            @endforeach
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            @foreach ($categories as $key => $category)
                                <div role="tabpanel" class="tab-pane fade @if($key == 0) in active @endif" id="{{ $category->id_category }}">
                                    <div class="body table-responsive">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Project Name</th>
                                                    <th>Date Added</th>
                                                    <th class="text-center">Status</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i = 0;
                                                @endphp

                                                @foreach ($projects as $keyz => $project)
                                                @if($category->id_category == $project->id_category)
                                                <tr>
                                                    <th scope="row">{{ $i + 1 }}</th>
                                                    <td>{{ $project->project_name }}</td>
                                                    <td>{{ date('d-M-Y', strtotime($project->created_at->toDateString())) }} | {{ $project->created_at->toTimeString() }}</td>
                                                    <td class="text-center">
                                                        <div class="switch">
                                                            <label>HIDE<input class="activate" type="checkbox" value="{{ $project->id_project }}" @if($project->is_active == 1) name="active" checked @elseif($project->is_active == 0) name="inactive" @endif><span class="lever"></span>SHOW</label>
                                                        </div>
                                                    </td>
                                                    <td class="text-center js-sweetalert">
                                                        <a href="/admin/editproject/{{ $project->id_project }}" type="button" class="btn btn-primary waves-effect">
                                                            <i class="material-icons">edit</i>
                                                            <span>EDIT</span>
                                                        </a>
                                                        <button type="button" class="btn btn-danger waves-effect" data-value="{{ $project->id_project }}" data-type="confirm">
                                                            <i class="material-icons">delete</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @php
                                                $i++;
                                                @endphp
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- #END# Striped Rows -->
    </div>
</section>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.0/dist/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function(){
        $('.activate').change(function(){
            var token = $('input[name="_token"]').val();
            var id = $(this).val();
            $.ajax({
                url:"{{ route('/admin/setActive') }}",
                method:"GET",
                data:{id:id,token:token},
                success: function(data) {
                    location.reload();
                }
            });
        });

        $('.js-sweetalert button').on('click', function () {
            var type = $(this).data('type');
            var id = $(this).data('value');
            var token = $('input[name="_token"]').val();
            if (type === 'confirm') {
                showConfirmMessage(id,token);
            }
        });

        function showConfirmMessage(id,token) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'Cancel',
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                    setTimeout(function() {
                        $.ajax({
                            url:"/admin/removeProject",
                            method:"GET",
                            data:{id:id,token:token},
                            success: function(response) {
                                if(response.status == 'ok') {
                                    $('.top-right').notify({
                                        message: { text: response.message }
                                    }).show();
                                    setTimeout(location.reload(), 2000);
                                } else {
                                    $('.top-right').notify({
                                        message: { text: response.message },
                                        type:'warning'
                                    }).show();
                                }
                                
                            }
                        })
                    }, 1000)
                };
            })
        }
    });
</script>
@stop