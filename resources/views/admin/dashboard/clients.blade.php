@extends('admin.layout.template')

@section('title')
    Clients
@stop

@section('extraCSS')
<link href="{{ URL::asset('cmsBSB/plugins/sweetalert/sweetalert2.min.css') }}" rel="stylesheet" />
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <!-- Image Gallery -->
        <div class="block-header">
            <div class="row">
                <div class="col-xs-6 pull-left">
                    <h3>CLIENTS</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Client List
                            <small>All the clients you want to show on homepage</a></small>
                        </h2>
                        <ul class="header-dropdown putOnRight m-r--5">
                            <!-- <div class="col-xs-3"> -->
                                <a type="button" data-toggle="modal" data-target="#add" class="btn btn-block btn-lg btn-primary waves-effect"><i class="material-icons">add_circle</i><span>ADD CLIENT</span></a>
                            <!-- </div> -->
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Client Logo</th>
                                    <th>Client Name</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Order Number</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class='sort-list js-sweetalert'>
                                <?php $i=1; ?>
                                
                                @foreach ($clients as $client)
                                <tr class="dataArray" data-index="{{ $client->id }}" data-position="{{ $client->order_number }}">
                                    <th scope="row">
                                        <?php echo $i; ?>.
                                    </th>
                                    <td style="width: 15%">
                                        <div class="col-lg-12" style="margin-bottom: 0;">
                                            <a href="{{ asset('/storage/clients/'.$client->img_file)}}" data-sub-html="Description">
                                                <img class="img-responsive thumbnail" style="margin-bottom: 0;" src="{{ asset('/storage/clients/thumb/'.$client->img_thumb_file)}}">
                                            </a>
                                        </div>
                                    </td>
                                    <td>{{ $client->name }}</td>
                                    <td class="text-center">
                                        @if ($client->is_active == 1)
                                        <span class="label bg-green">Active</span>
                                        @else
                                        <span class="label bg-deep-orange">Inactive</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{ $client->order_number }}
                                        <input class='originalposition' type='hidden' name='item[origposition]' value='{{ $client->order_number }}'>
                                        <input class='currentposition' type='hidden' name='currposition' value='{{ $client->order_number }}'>
                                    </td>
                                    <td class="text-center">
                                        <div class="switch">
                                            <label>HIDE<input class="activate" name="radio" type="checkbox" value="{{ $client->id }}" @if($client->is_active == 1) name="active" checked @elseif($client->is_active == 0) name="inactive" @endif><span class="lever"></span>SHOW</label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <a href="/admin/editclients/{{ $client->id }}" type="button" class="btn btn-primary waves-effect">
                                            <i class="material-icons">edit</i>
                                            <span>EDIT</span>
                                        </a>
                                        <button type="button" class="btn btn-danger waves-effect" data-value="{{ $client->id }}" data-type="confirm">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form action="/admin/addNewClient" name="submission" class="dropzone" id="frmFileUpload" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Add Client</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group form-float">
                                <div class="form-line editForm">
                                    <input type="text" name="clientName" value="" class="form-control">
                                    <label class="form-label">Client Name</label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="idDataToSave" value="" class="idDataToSave" id="idDataToSave">
                    </div>
                    <div id="dropzoneDragArea">
                        <div class="dz-message">
                            <div class="drag-icon-cph">
                                <i class="material-icons">touch_app</i>
                            </div>
                            <h3>Drag and Drop Images or click here to upload.</h3>
                            <em>(Upload maximum is <strong>1 file</strong> only)</em>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.0/dist/sweetalert2.all.min.js"></script>
<script src="{{ URL::asset('cmsBSB/js/uploadclient.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.activate').change(function(){
            var token = $('input[name="_token"]').val();
            var id = $(this).val();
            $.ajax({
                url:"{{ route('/admin/toggleClient') }}",
                method:"GET",
                data:{id:id,token:token},
                success: function(data) {
                    location.reload();
                }
            });
        });
    });

    $('.js-sweetalert button').on('click', function () {
        var type = $(this).data('type');
        var id = $(this).data('value');
        var token = $('input[name="_token"]').val();
        if (type === 'confirm') {
            showConfirmMessage(id,token);
        }
    });

    function showConfirmMessage(id,token) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success',
                )
                setTimeout(function() {
                    $.ajax({
                        url:"{{ route('/admin/removeClient') }}",
                        method:"GET",
                        data:{id:id,token:token},
                        success: function(data) {
                            location.reload();
                        }
                    })
                }, 1000)
            };
        })
    }

    $('.sort-list').sortable({
        axis: 'y',
        opacity: 0.7,
        cursor: 'pointer',
        update: function (event, ui) {
            $(this).children().each(function (index) {
                if ($(this).attr('data-position') != (index+1)) {
                    $(this).attr('data-position', (index+1)).addClass('updated');
                }
            });

            saveNewPositions();
        }
    });
    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function () {
            positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
            $(this).removeClass('updated');
        });

        $.ajax({
            url: "{{ url('/admin/setClientOrder') }}",
            method: 'GET',
            dataType: 'text',
            data: {
                update: 1,
                positions: positions
            }, success: function (response) {
                location.reload();
            }
        });
    }
</script>
@stop