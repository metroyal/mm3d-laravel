@extends('admin.layout.template')

@section('title')
    Image Carousel
@stop

@section('extraCSS')
<link href="{{ URL::asset('cmsBSB/plugins/sweetalert/sweetalert2.min.css') }}" rel="stylesheet" />
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <!-- Image Gallery -->
        <div class="block-header">
            <div class="row">
                <div class="col-xs-6 pull-left">
                    <h3>CAROUSEL</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Pictures List
                            <small>All the slideshow in the frontpage of your website</a></small>
                        </h2>
                        <ul class="header-dropdown putOnRight m-r--5">
                            <!-- <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li> -->
                            <!-- <div class="col-xs-3"> -->
                                <a href="/admin/newcarousel" type="button" class="btn btn-block btn-lg btn-primary waves-effect"><i class="material-icons">add_circle</i><span>ADD IMAGE</span></a>
                            <!-- </div> -->
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Image</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Order Number</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            {{ csrf_field() }}
                            <tbody class='sort-list js-sweetalert'>
                                <?php $i=1; ?>
                                
                                @foreach ($images as $image)
                                <tr class="dataArray" data-index="{{ $image->id }}" data-position="{{ $image->order_number }}">
                                    <th scope="row">
                                        <?php echo $i; ?>.
                                    </th>
                                    <td>
                                        <div class="col-lg-3" style="margin-bottom: 0;">
                                            <a href="{{ asset('/storage/carousel/'.$image->url_img)}}" data-sub-html="Description">
                                                <img class="img-responsive thumbnail" style="margin-bottom: 0;" src="{{ asset('/storage/carousel/'.$image->url_thumb)}}">
                                            </a>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        @if ($image->is_active == 1)
                                        <span class="label bg-green">Active</span>
                                        @else
                                        <span class="label bg-deep-orange">Inactive</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{ $image->order_number }}
                                        <input class='originalposition' type='hidden' name='item[origposition]' value='{{ $image->order_number }}'>
                                        <input class='currentposition' type='hidden' name='currposition' value='{{ $image->order_number }}'>
                                    </td>
                                    <td class="text-center">
                                        <div class="switch">
                                            <label>HIDE<input class="activate" name="radio" type="checkbox" value="{{ $image->id }}" @if($image->is_active == 1) name="active" checked @elseif($image->is_active == 0) name="inactive" @endif><span class="lever"></span>SHOW</label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-danger waves-effect" data-value="{{$image->id}}" data-type="confirm">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="body">
                        <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                            @foreach ($images as $image)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <a href="{{ asset('cmsBSB/images/carousel/'.$image->url_img)}}" data-sub-html="Demo Description">
                                    <img class="img-responsive thumbnail" src="{{ asset('cmsBSB/images/carousel/'.$image->url_thumb)}}">
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.0/dist/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function(){
        $('.activate').change(function(){
            var token = $('input[name="_token"]').val();
            var id = $(this).val();
            $.ajax({
                url:"{{ url('/admin/switchCarousel') }}",
                method:"GET",
                data:{id:id,token:token},
                success: function(data) {
                    location.reload();
                    console.log(data);
                }
            });
        });
    });

    $('.js-sweetalert button').on('click', function () {
        var type = $(this).data('type');
        var id = $(this).data('value');
        var token = $('input[name="_token"]').val();
        if (type === 'confirm') {
            showConfirmMessage(id,token);
        }
    });

    function showConfirmMessage(id,token) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success',
            )
            setTimeout(function() {
                $.ajax({
                    url:"{{ route('/admin/deleteCarousel') }}",
                    method:"GET",
                    data:{id:id,token:token},
                    success: function(data) {
                        location.reload();
                    }
                })
            }, 1000)};
        })
    }

    $('.sort-list').sortable({
        axis: 'y',
        opacity: 0.7,
        cursor: 'pointer',
        update: function (event, ui) {
            $(this).children().each(function (index) {
                if ($(this).attr('data-position') != (index+1)) {
                    $(this).attr('data-position', (index+1)).addClass('updated');
                }
            });

            saveNewPositions();
        }
    });
    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function () {
            positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
            $(this).removeClass('updated');
        });

        $.ajax({
            url: "{{ route('/admin/setCarouselOrder') }}",
            method: 'GET',
            dataType: 'text',
            data: {
                update: 1,
                positions: positions
            }, success: function (response) {
                location.reload();
            }
        });
    }
</script>
@stop