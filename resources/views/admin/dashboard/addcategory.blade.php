@extends('admin.layout.template')

@section('title')
    Add new Category
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <!-- Horizontal Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ADD CATEGORY
                        </h2>
                    </div>
                    <div class="body">
                    <!-- @if(session()->has('message'))
                    <div class="alert alert-success alertSubmit animated">
                        {{ session()->get('message') }}
                    </div>
                    @endif -->
                        <form method="post" action="{{ url('/admin/addnewcategory') }}">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    @foreach ($categories as $category)
                                    {{ $category->id_category }}. {{ $category->category_name }}<br>
                                    @endforeach
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="categoryName" value="{{ old('categoryname') }}" class="form-control">
                                            <label class="form-label">Category Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line editCategoryType">
                                            <select id="categoryType" name="categoryType" class="form-control show-tick">
                                                <option value="">-- Select Category Type --</option>
                                                <option value="video">Video</option>
                                                <option value="image">Image</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-12 noMargin">
                                    <button type="submit" class="btn bg-primary waves-effect">
                                        <i class="material-icons">save</i>
                                        <span>SAVE</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Horizontal Layout -->
    </div>
</div>
@stop