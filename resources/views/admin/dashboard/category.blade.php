@extends('admin.layout.template')

@section('title')
    Category List
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-sm-12 col-sm-6">
                    <h3>CATEGORIES LIST</h3>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            MM3D - CATEGORIES LIST
                            <small>Daftar kategori project</small>
                        </h2>
                        <ul class="col-lg-3 col-md-3 col-sm-6 col-xs-12 header-dropdown m-r--5">
                            <a type="button" href="newcategory" class="btn btn-block btn-lg btn-primary waves-effect"><i class="material-icons" style="color: #fff!important;">add_circle</i><span>ADD CATEGORY</span></a>
                        </ul>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Category Name</th>
                                    <th class="text-center">Content</th>
                                    <th class="text-center">Created Date</th>
                                    <th class="text-center">Last Update</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class="js-sweetalert">
                                {{ csrf_field() }}
                                @foreach ($categories as $category)
                                <tr>
                                    <th scope="row" class="text-center">{{ $category->id_category }}</th>
                                    <td>{{ $category->category_name }}</td>
                                    <td class="text-center">
                                        @if($category->category_type == 'video')
                                        <i class="material-icons">video_library</i><span style="position: relative; top: -8px; left: 7px;">Video</span>
                                        @elseif($category->category_type == 'image')
                                        <i class="material-icons">photo_camera</i><span style="position: relative; top: -8px; left: 7px;">Image</span>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $category->created_at }}</td>
                                    <td class="text-center">{{ $category->updated_at }}</td>
                                    <td class="text-center">
                                        <a href="#" type="button" data-toggle="modal" data-target="#edit" data-edit="{{$category->id}}" onclick="edit({{$category->id}})" class="btn btn-primary waves-effect">
                                            <i class="material-icons">edit</i>
                                            <span>EDIT</span>
                                        </a>
                                        <button type="button" class="btn btn-danger waves-effect" data-value="{{$category->id}}" data-type="confirm">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Striped Rows -->
        <div class="modal fade" id="edit" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <!-- <form method="POST"> -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Edit Category Name</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group form-float">
                                <div class="form-line editCategoryName">
                                    <input type="text" name="categoryName" value="" class="form-control">
                                    <label class="form-label">Category Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line editCategoryType">
                                    <select id="categoryType" name="categoryType" class="form-control show-tick">
                                        <option value="">-- Select Category Type --</option>
                                        <option value="video">Video</option>
                                        <option value="image">Image</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary waves-effect" data-type="saveCategory" onclick="update()">UPDATE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.0/dist/sweetalert2.all.min.js"></script>
<script>
    function edit(id) { // TODO  
        $('.editCategoryName').addClass('focused');
        $('.editCategoryType').addClass('focused');
        window.idCategory = id;
        $.ajax({
            url: "{{ url('/admin/getCategoryById/') }}" + '/' + id,
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                $('input[name="categoryName"]').val(response.name);
                $('#categoryType').selectpicker('val', response.type);
            }
        });
    }

    function update() { // TODO
        id = idCategory;
        token = $("input[name='_token']").val();
        categoryName = $("input[name='categoryName']").val();
        categoryType = $('#categoryType').selectpicker().val();
        $.ajax({
            url: "{{ url('/admin/updateCategory/') }}" + '/' + id,
            method: 'GET',
            dataType: 'json',
            data: {
                token:token,
                categoryName:categoryName,
                categoryType:categoryType
            },
            success: function (response) {
                if(response.status == 'ok'){
                    location.reload();
                } else if (response.status == 'errors') {
                    $.each(response.message, function(key,value) {
                        $('.top-right').notify({
                            message: { text: value },
                            type:'info'
                        }).show();
                    });
                } else if (response.status == 'error') {
                    $('.top-right').notify({
                        message: { text: response.message },
                        type:'info'
                    }).show();
                }
            }
        });
    }

    $(document).ready(function(){
        $('.activate').change(function(){
            var token = $('input[name="_token"]').val();
            var id = $(this).val();
            $.ajax({
                url:"{{ route('/admin/setActive') }}",
                method:"GET",
                data:{id:id,token:token},
                success: function(data) {
                    console.log('data: ', data);
                    location.reload();
                }
            });

        });
    });

    $('.js-sweetalert button').on('click', function () {
        var type = $(this).data('type');
        var id = $(this).data('value');
        var token = $('input[name="_token"]').val();
        if (type === 'confirm') {
            showConfirmMessage(id,token);
        }
    });

    function showConfirmMessage(id,token) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success',
            )
            setTimeout(function() {
                $.ajax({
                    url:"{{ route('/admin/removeCategory') }}",
                    method:"GET",
                    data:{id:id,token:token},
                    success: function(data) {
                        location.reload();
                    }
                })
            }, 1000)};
        })
    }
</script>
@stop