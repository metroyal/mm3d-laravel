@extends('admin.layout.template')

@section('title')
    Edit Project
@stop

@section('extraCSS')
<!-- <link href="{{ URL::asset('cmsBSB/plugins/fileuploaderjs/css/custom.css') }}" rel="stylesheet"/> -->

@endsection

@section('body')
<section class="content">
    <div class="container-fluid">
        <!-- Horizontal Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            EDIT PROJECT
                        </h2>
                    </div>
                    <div class="body">
                    <!-- @if(session()->has('message'))
                    <div class="alert alert-success alertSubmit animated">
                        {{ session()->get('message') }}
                    </div>
                    @endif class="dropzone"-->
                        <form id="frmFileUpload" method="POST" action="{{ url('admin/storeeditproject/'.$project->id_project) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div>
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" name="projectname" value="{{ $project->project_name }}" class="form-control">
                                                <label class="form-label">Project Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="category" class="form-control show-tick">
                                            <option value="">-- Select Project Category --</option>
                                            @foreach ($categories as $category)
                                            <option value="{{ $category->id_category }}" data-type="{{ $category->category_type }}" {{ ($category->id_category == $project->id_category ? "selected":"") }}>{{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="categorytype" value="{{ $category->category_type }}">
                                    </div>
                                    <div id="videoUpload" class="col-sm-12" @if($project->category_type == 'image') style="display: none" @endif>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <h2 class="card-inside-title">YouTube Embed</h2>
                                                <textarea name="youtubeLink" id="editor" rows="10" cols="80">
                                                    {{ $project->youtube_link }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="imageUpload" class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <h2 class="card-inside-title">Project Image</h2>
                                            <input type="file" name="thumbProject" value="{{ $project->thumb_project }}">
                                            <br>
                                            <img src="{{ asset('/storage/project/thumb/'.$project->thumb_project) }}" width="auto" height="130"> 
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group input-group-lg">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" class="filled-in" name="use360" id="ig_checkbox" value="1" {{ ($project->use360 == "1" ? "checked":"") }}>
                                                    <label for="ig_checkbox"></label>
                                                </span>
                                                <span type="text" class="form-control">Use 360 Virtual Tour</span>
                                            </div>
                                        </div>
                                        <div id="input360" style="display: {{ ($project->use360 == '1' ? 'block':'none') }}" class="form-group form-float">
                                            <h2 class="card-inside-title">Upload 360 File</h2>
                                            <input type="file" name="360file" value="">
                                            @if($panoimage)
                                            <br>
                                            <img src="{{ asset('/storage/360image/'.$panoimage['file_name']) }}" width="auto" height="130"> 
                                            @else
                                            <p>Empty</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-12 noMargin text-center">
                                    <button type="submit" class="btn bg-primary waves-effect">
                                        <i class="material-icons">save</i>
                                        <span>SAVE THIS UPDATES</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Horizontal Layout -->
    </div>
</div>
@endsection

@section('script')
<script src="{{ URL::asset('cmsBSB/plugins/ckeditor4/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'editor' );

    $('[name="category"]').on('change', function() {
        var type = $(this.options[this.selectedIndex]).attr('data-type');
        if(type == 'image') {
            $('#imageUpload').slideDown();
            $('#videoUpload').slideUp();
        } else if(type == 'video') {
            $('#imageUpload').slideDown();
            $('#videoUpload').slideDown();
        }
        $('[name="categorytype"]').val(type);
    })

    $(document).ready(function(){
        $('#ig_checkbox').change(function(){
            if (this.checked) {
                $('#input360').fadeIn('slow');
            }
            else {
                $('#input360').fadeOut('slow');
            }                   
        });
    });
</script>

<!-- <script src="{{ URL::asset('cmsBSB/js/pages/forms/advanced-form-elements.js') }}"></script> -->

@endsection