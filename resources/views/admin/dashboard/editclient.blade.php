@extends('admin.layout.template')

@section('title')
    Clients
@stop

@section('extraCSS')
<link href="{{ URL::asset('cmsBSB/plugins/sweetalert/sweetalert2.min.css') }}" rel="stylesheet" />
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <!-- Image Gallery -->
        <div class="block-header">
            <div class="row">
                <div class="col-xs-6 pull-left">
                    <h3>CLIENTS</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Edit Client
                            <small>Change client name and logo</a></small>
                        </h2>
                        <!-- <ul class="header-dropdown putOnRight m-r--5"> -->
                            <!-- <div class="col-xs-3"> -->
                                <!-- <a type="button" data-toggle="modal" data-target="#add" class="btn btn-block btn-lg btn-primary waves-effect"><i class="material-icons">add_circle</i><span>ADD IMAGE</span></a> -->
                            <!-- </div> -->
                        <!-- </ul> -->
                    </div>
                    <div class="body">
                        <form name="submission" method="POST" id="frmFileUpload" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group form-float">
                                <div class="form-line editForm">
                                    <input type="text" name="clientName" value="{{ $client->name }}" class="form-control">
                                    <label class="form-label">Client Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <div id="currentImage" class="row">
                                        <div class="col-xs-6">
                                            <div class="thumbnail">
                                                <img src="{{ asset('/storage/clients/thumb/'.$client->img_thumb_file)}}" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-danger waves-effect" data-type="confirm" onclick="deleteCurrentImage()">
                                                <i class="material-icons">delete</i>
                                                <span>DELETE IMAGE</span>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="idDataToSave" value="" class="idDataToSave" id="idDataToSave">
                                    <input type="hidden" name="fileNameToSave" value="" class="fileNameToSave" id="fileNameToSave">
                                </div>
                            </div>
                            <div class="form-group">
                                <div action="/admin/updateClient/{{$client->id}}" name="submission" class="dropzone" id="dropzoneContainer">
                                    <div id="dropzoneDragArea">
                                        <div class="dz-message">
                                            <div class="drag-icon-cph">
                                                <i class="material-icons">touch_app</i>
                                            </div>
                                            <h3>Drag and Drop Images or click here to upload.</h3>
                                            <em>(Upload maximum is <strong>1 file</strong> only)</em>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button id="updateButton" type="submit" class="btn btn-primary waves-effect">UPDATE</button>
                            <a href="/admin/clients" id="back" type="button" class="btn btn-link waves-effect">BACK</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.0/dist/sweetalert2.all.min.js"></script>
<script>
    Dropzone.autoDiscover = false;
    let token = $('input[name="_token"]').val();
    $(function() {
        myDropzone = new Dropzone('.dropzone',{
            url: "/admin/uploadClientLogo",
            autoProcessQueue: false,
            parallelUploads: 1,
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 1,
            addRemoveLinks: true,
            dictRemoveFile: "Hapus",
            params: {
                _token: token
            },
            init: function(){
                myDropzone = this;

                $("form[name='submission']").submit(function(event){
                    event.preventDefault();
                    if($('input[name="clientName"]').val() != "{{ $client->name }}" || myDropzone.files.length) {
                        if($("input[name='clientName']").val() == '') {
                            $('.top-right').notify({
                                message: 'Fill client name',
                                type:'warning'
                            }).show();
                        } else {
                            URL = $('#dropzoneContainer').attr('action');
                            formData = $('#frmFileUpload').serialize();
                            $.ajax({
                                type: 'POST',
                                url: URL,
                                data: formData,
                                success: function(result) {
                                    if(result.status == "ok") {
                                        var id = result.idRow;
                                        var fileName = result.imgFile;
                                        $('#idDataToSave').val(id);
                                        $('#fileNameToSave').val(fileName);
                                        myDropzone.processQueue();
                                    } else {
                                        $('.top-right').notify({
                                            message: 'Failed!',
                                            type:'warning'  
                                        }).show();
                                    }
                                }
                            });
                            if($('input[name="clientName"]').val() != "{{ $client->name }}" || !myDropzone.files.length){
                                location.reload();
                            }
                        }
                    } else {
                        $('.top-right').notify({
                            message: "No Change OR no new image!",
                            type:'info'
                        }).show();
                    }
                });
                
                this.on("sending", function(file, xhr, formData) {
                    let id = document.getElementById('idDataToSave').value;
                    let fileName = document.getElementById('fileNameToSave').value;
                    formData.append('idDataToSave', id);
                    formData.append('fileNameToSave', fileName);
                });

                
                this.on("complete", function(){
                    $('#frmFileUpload')[0].reset();
                    $('.modal').modal('hide');
                    location.reload();
                });
            }
        });
    });

    $('#dropzoneContainer').hide();

    function deleteCurrentImage() {
        $('#currentImage').hide();
        $('#dropzoneContainer').show();
    }

    function update() {
        id = idClient;
        token = $("input[name='_token']").val();
        clientName = $("input[name='clientName']").val();
        $.ajax({
            url: "{{ url('/admin/updateClient/') }}" + '/' + id,
            method: 'GET',
            dataType: 'json',
            data: {
                token:token,
                clientName:clientName,
                file: null
            },
            success: function (response) {
                if(response.status == 'ok'){
                    location.reload();
                } else if (response.status == 'error') {
                    $('.top-right').notify({
                        message: { text: response.message },
                        type:'info'
                    }).show();
                }
            }
        });
    }

    $(document).ready(function(){
        $('.activate').change(function(){
            var token = $('input[name="_token"]').val();
            var id = $(this).val();
            $.ajax({
                url:"{{ route('/admin/switchCarousel') }}",
                method:"GET",
                data:{id:id,token:token},
                success: function(data) {
                    location.reload();
                }
            });
        });
    });

    $('.js-sweetalert button').on('click', function () {
        var type = $(this).data('type');
        var id = $(this).data('value');
        var token = $('input[name="_token"]').val();
        if (type === 'confirm') {
            showConfirmMessage(id,token);
        }
    });

    function showConfirmMessage(id,token) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success',
            )
            setTimeout(function() {
                $.ajax({
                    url:"{{ route('/admin/deleteCarousel') }}",
                    method:"GET",
                    data:{id:id,token:token},
                    success: function(data) {
                        location.reload();
                    }
                })
            }, 1000)};
        })
    }

    $('.sort-list').sortable({
        axis: 'y',
        opacity: 0.7,
        cursor: 'pointer',
        update: function (event, ui) {
            $(this).children().each(function (index) {
                if ($(this).attr('data-position') != (index+1)) {
                    $(this).attr('data-position', (index+1)).addClass('updated');
                }
            });

            saveNewPositions();
        }
    });
    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function () {
            positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
            $(this).removeClass('updated');
        });

        $.ajax({
            url: "{{ route('/admin/setClientOrder') }}",
            method: 'GET',
            dataType: 'text',
            data: {
                update: 1,
                positions: positions
            }, success: function (response) {
                location.reload();
            }
        });
    }
</script>
@stop