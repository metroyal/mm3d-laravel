@extends('admin.layout.template')

@section('title')
    Add New Slideshow
@stop

@section('body')
<section class="content">
    <div class="container-fluid">
        <!-- Horizontal Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ADD IMAGE TO PROJECT
                        </h2>
                    </div>
                    <div class="body">
                    <!-- @if(session()->has('message'))
                    <div class="alert alert-success alertSubmit animated">
                        {{ session()->get('message') }}
                    </div>
                    @endif -->
                        <div>
                            <h2 class="card-inside-title">Upload Image Project</h2>
                            <div>
                                <form action="/admin/actionprojectimage" class="dropzone" method="post" id="frmFileUpload" enctype="multipart/form-data">
                                    @csrf
                                    <div class="dz-message">
                                        <div class="drag-icon-cph">
                                            <i class="material-icons">touch_app</i>
                                        </div>
                                        <h3>Drag and Drop Images of your project or click here to upload.</h3>
                                        <em>(Parallel upload maximum is <strong>4 files</strong> on the same time)</em>
                                    </div>
                                    <div class="fallback">
                                        <input name="image" type="file" multiple/>
                                    </div>
                                    <input name="idproject" type="hidden" value="{{ $project->id_project }}"/>
                                </form>
                                <div class="clearfix">
                                    <div class="col-sm-12 noMargin text-center mt-20">
                                        <button type="submit" class="btn bg-primary waves-effect process">
                                            <i class="material-icons">save</i>
                                            <span>UPLOAD</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Horizontal Layout -->
    </div>
</div>
@endsection

@section('script')
<script src="{{ URL::asset('cmsBSB/js/uploadprojectimage.js') }}"></script>
@stop