@extends('template')

@section('title')
    MM3D - Project
@stop

@section('css')
<link rel="stylesheet" href="{{URL::asset('css/style.css')}}" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="{{URL::asset('css/projectDetail.css')}}" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="{{URL::asset('css/ekko-lightbox.css')}}" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="https://cdn.pannellum.org/2.4/pannellum.css"/>
@endsection

@section('content')
<div class="main-header darken-menu">
    @include('layouts.nav')
</div>
<div id="home" class="navbar-wrapper headerProject" style="background-image: url(../storage/project/{{$project['header_image']}})"></div>
<div class="header">
    <div class="caption">
        <h1 data-sr="enter left move 200px, over 1s, wait 0.5s">{{$project['project_name']}}</h1>
        <div class="appscreen" data-sr="move bottom move 300px, vFactor 0.4, over 1s, wait 1s">
            <!-- <img src="" width="1125" height="606" alt="AppScreen"> -->
        </div>
    </div>
</div>

<!-- Intro & Role
================================================== -->

<section class="intro-ux">
    <div class="container underline">
        <div class="row no-padding">
            <div class="section_title mt-50">
                <h1>{{$project['project_name']}}</h1>
                @if($project['use360'] == '1')
                    <h2>360&#176; Virtual Tour</h2>
                    <div id="panorama"></div>
                @endif
                <h2>Project Details</h2>
                <p class="passage-dark">{!! $project['description'] !!}</p>
                @if($project['youtube_link'] != null)
                <div class="videoContainer">
                    <div class="video">
                        {!! $project['youtube_link'] !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container content">
        <div class="row no-padding">
            <div class="col-md-12">
                @foreach ($galleries as $gallery)
                <figure class="col-md-4 no-padding">
                    <a href="{{ '/storage/image-gallery/'.$gallery->file_name }}" data-toggle="lightbox" data-gallery="mm3d" data-type="image">
                        <img alt="picture" src="{{ '/storage/image-gallery/thumb/thumb_'.$gallery->file_name }}"
                        class="img-fluid">
                    </a>
                </figure>
                @endforeach
            </div>
        </div>
    </div>
</section>

<div class="container">
    <footer class="col-md-12 mb-20">
        <p class="logo-footer">
            &middot; &copy; <font color="#f8941e">MM 3D</font>
            <script>
                document.querySelector('.logo-footer').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>
            &middot;
        </p>
    </footer>
</div>

<!-- Result
================================================== -->

<!-- <section class="result-ux">
    <div class="container">
        <div class="row">
            <div class="result-wrapper">
                <div class="section_title">
                    <h3>Final Result</h3>
                    <p>An application that really engage and address users needed.</p>
                    <div class="phone" data-sr="move bottom move 200px, vFactor 0.4, over 1s"><img src="img/phone.png" width="917" height="824" alt="Phone"></div>
                </div>
            </div>
            <div>
                <div class="footer">
                    <button class="col-xs-6 left" onclick="window.history.back()" ><i class="fa fa-arrow-circle-left" style="margin-right: 10px;"></i>homepage</button>
                    <h4 class="col-xs-6 right">next project<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></h4>
                </div>
            </div>
        </div>
    </div>
</section> -->
@endsection

@section('js')
<script src="{{URL::asset('js/ekko-lightbox.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="https://cdn.pannellum.org/2.4/pannellum.js"></script>
<script>
    pannellum.viewer('panorama', {
        "type": "equirectangular",
        "panorama": "{{$project['file_name']}}",
        "basePath": "{{URL::asset('/storage/360image/')}}/",
        "showControls": true,
        "autoRotate": -7,
        "autoLoad": true
    });
</script>
<script type="text/javascript">
    $(document).ready(function ($) {
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            return $(this).ekkoLightbox({});
        });
    });
</script>
@endsection