<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categories', 'ApiController@getCategories');
Route::get('projects', 'ApiController@getProjects');
Route::get('project/{class}', 'ApiController@getProjectsByCategory');
// set $contact active
Route::post('contact/setActive/{id}', 'AdminController@setContactActiveStatus');
// delete $contact
Route::delete('contact/delete/{id}', 'AdminController@deleteContact');
