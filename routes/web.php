<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// create link for Artisan command to optimize clear
Route::get('/linkoptimize', function () {
    Artisan::call('optimize:clear');
    return "Optimize Clear Done!";
});

Route::get('/linkstorage', function () {
    symlink('/home/mmdarsit/dev.mm3darsitektur.com/engine/storage/app/public',  '/home/mmdarsit/dev.mm3darsitektur.com/storage');
    return dd('Storage Link Created!');
});

//WEB PAGE ROUTING
Route::get('/','AdminController@home');
// Route::get('/project/{id_project}','AdminController@projectDetail');
Route::get('/projects', function() {
    return view('projects-view');
});

Route::get('/email', function() {
    return view('mail.html-template');
});

Route::get('/projects/{category}', function() {
    return view('projects-view');
});

//USERS ADMIN ROUTING
Route::get('/admin/login','AdminController@signin')->middleware('loggedin');
Route::get('/admin/register','AdminController@register');
Route::get('/admin/forgot','AdminController@forgotpass');
Route::get('/logout','AdminController@logout');

//ADMIN INTERFACE ROUTING
Route::get('/admin','AdminController@dashboard')->middleware('authuser');
Route::get('/admin/projects','AdminController@projectlist')->middleware('authuser');
Route::get('/admin/newproject','AdminController@addproject')->middleware('authuser');
Route::get('/admin/editproject/{id_project}','AdminController@editproject')->middleware('authuser');
Route::get('/admin/addprojectimage/{id_project}','AdminController@addprojectimage')->middleware('authuser');
Route::get('/admin/slideshow','AdminController@carousel')->middleware('authuser');
Route::get('/admin/newcarousel','AdminController@addcarousel')->middleware('authuser');
Route::get('/admin/category','AdminController@category')->middleware('authuser');
Route::get('/admin/newcategory','AdminController@newcategory')->middleware('authuser');
Route::get('/admin/sortcarousel/{orderNumber}/{oldNumber}/{newNumber}','AdminController@sortcarousel')->middleware('authuser');
Route::get('/admin/testimonial/','AdminController@viewTestimonial')->middleware('authuser');
Route::get('/admin/edittestimonial/{idTesti}','AdminController@getEditTestimonial')->middleware('authuser');
Route::get('/admin/editcontact/{id}','AdminController@getEditContactProfile')->middleware('authuser');
Route::get('/admin/editprofile','AdminController@getContactProfile')->middleware('authuser');
Route::get('/admin/clients','AdminController@viewClient')->middleware('authuser');
Route::get('/admin/editclients/{id}','AdminController@getClientById')->middleware('authuser');

//DATA
Route::post('/admin/storeproject', 'AdminController@storeproject')->middleware('authuser');
Route::post('/admin/storeeditproject/{id}','AdminController@storeEditProject')->middleware('authuser');
Route::get('/admin/removeProject','AdminController@removeProject')->middleware('authuser');
Route::post('/admin/registeruser', 'AdminController@registeruser');
Route::post('/admin/dologin', 'AdminController@dologin');
Route::get('/admin/setActive', 'AdminController@setActive')->name('/admin/setActive');
Route::get('/admin/setCarouselOrder', 'AdminController@setCarouselOrder')->name('/admin/setCarouselOrder');
Route::get('/admin/setCarouselActive', 'AdminController@setCarouselActive')->name('/admin/switchCarousel');
Route::get('/admin/removeCarousel', 'AdminController@removeCarousel')->name('/admin/deleteCarousel');
Route::post('/admin/uploadcarousel', 'AdminController@UploadCarousel');
Route::post('/admin/uploadcarouselTest', 'AdminController@UploadCarouselTest');
Route::post('/admin/addnewcategory','AdminController@addnewcategory');
Route::get('/admin/getCategoryById/{idCategory}','AdminController@getCategoryById');
Route::get('/admin/updateCategory/{idCategory}','AdminController@updateCategory')->middleware('authuser');
Route::get('/admin/removeCategory','AdminController@removeCategory')->name('/admin/removeCategory')->middleware('authuser');
Route::post('/admin/postimage','AdminController@addprojectimage');
Route::post('/admin/actionprojectimage','AdminController@actionprojectimage');
Route::post('/admin/storeTestimonial','AdminController@addTestimonial')->middleware('authuser');
Route::get('/admin/setTestimonialOrder', 'AdminController@setTestimonialOrder')->name('/admin/setTestiOrder');
Route::get('/admin/removeTestimonial', 'AdminController@removeTestimonial')->name('/admin/deleteTesti');
Route::get('/contactus','AdminController@sendEmail');
Route::get('/admin/updatetesti/{idTesti}','AdminController@updateTestimonial')->middleware('authuser');
Route::post('/admin/storeProfile','AdminController@addContactProfile')->middleware('authuser');
Route::get('/admin/edittestimonial/{idTesti}','AdminController@getEditTestimonial')->middleware('authuser');
Route::get('/admin/storeeditcontact/{id}','AdminController@storeEditContactProfile')->middleware('authuser');
Route::post('/admin/addNewClient','AdminController@addClient')->middleware('authuser');
Route::post('/admin/uploadClientLogo','AdminController@UploadClientImage')->middleware('authuser');
Route::get('/admin/getClient/{id}','AdminController@getClientById')->middleware('authuser');
Route::post('/admin/updateClient/{id}','AdminController@updateClient')->middleware('authuser');
Route::get('/admin/setClientOrder', 'AdminController@setClientOrder')->name('/admin/setClientOrder');
Route::get('/admin/toggleClientActive', 'AdminController@toggleClientActive')->name('/admin/toggleClient');
Route::get('/admin/removeClient', 'AdminController@deleteClient')->name('/admin/removeClient');

Route::post('/admin/fileuploader', 'AdminController@submit');
Route::post('/admin/ajax_remove_file', 'AdminController@removeFile');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

// Auth::routes();