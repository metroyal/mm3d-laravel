Dropzone.autoDiscover = false;
let token = $('input[name="_token"]').val();
$(function() {
    myDropzone = new Dropzone('.dropzone',{
        url: "/admin/uploadClientLogo",
        autoProcessQueue: false,
        parallelUploads: 1,
        paramName: "file",
        dictFileTooBig: `File is too big {{filesize}} MB. Max filesize: {{maxFilesize}} MB.`,
        maxFiles: 1,
        maxFilesize: 1,
        addRemoveLinks: true,
        dictRemoveFile: "Hapus",
        params: {
            _token: token
        },
        init: function(){
            myDropzone = this;
            $("form[name='submission']").submit(function(event){
                event.preventDefault();
                if($("input[name='clientName']").val() == '') {
                    $('.top-right').notify({
                        message: 'Fill client name!',
                        type:'warning'
                    }).show();
                } else if(!myDropzone.files.length){
                    $('.top-right').notify({
                        message: 'Add client logo!',
                        type:'warning'
                    }).show();
                } else {
                    URL = $('#frmFileUpload').attr('action');
                    formData = $('#frmFileUpload').serialize();
                    $.ajax({
                        type: 'POST',
                        url: URL,
                        data: formData,
                        success: function(result) {
                            if(result.status == "ok") {
                                var id = result.idRow;
                                $('#idDataToSave').val(id);
                                myDropzone.processQueue();
                            } else {
                                $('.top-right').notify({
                                    message: 'Failed!',
                                    type:'warning'  
                                }).show();
                            }
                        }
                    });
                }
            });
            this.on("sending", function(file, xhr, formData) {
                let id = document.getElementById('idDataToSave').value;
                formData.append('idDataToSave', id);
            });

            this.on("success", function(){
                $('#frmFileUpload')[0].reset();
                $('.modal').modal('hide');
                location.reload();
            });
        }
    });
});