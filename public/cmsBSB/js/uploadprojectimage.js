$(document).ready(function(){
    Dropzone.options.frmFileUpload = {
        url: null,
        autoProcessQueue: false,
        parallelUploads: 100,
        paramName: "image",
        // uploadMultiple: true,
        maxFiles: 100,
        maxFilesize: 1,
        dictFileTooBig: `File is too big {{filesize}} MB. Max filesize: {{maxFilesize}} MB.`,
        addRemoveLinks: true,
        dictRemoveFile: "Hapus",
        init: function(){
            var submitButton = document.querySelector('.process');
            myDropzone = this;
            submitButton.addEventListener("click", function(){
                Dropzone.forElement(".dropzone").processQueue();
                // myDropzone.removeAllFiles(file);
            });
            this.on("successmultiple", function() {
                alert("success multiple.");
            });
            this.on("complete", function(){
                if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                {
                    // window.location.href='../editproject/8';
                    // var token = $('input[name="_token"]').val();
                    // $.ajax({
                    //     url:"{{ route('admin/storeproject') }}",
                    //     method:"POST",
                    //     data:{token:token},
                    //     success: function(data) {
                    //         console.log('data: ', data);
                    //         // location.reload();
                    //     }
                    // });
                };
            });
        }
    };
});