<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $primaryKey = 'id';
    protected $table = 'category';
    protected $fillable = ['id','id_category','category_type','category_name','class'];
}