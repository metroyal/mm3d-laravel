<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model {
    protected $primaryKey = 'id';
    protected $table = 'testimonial';
    protected $fillable = ['id','name','testimonial','rate','is_active','order_number','is_deleted'];
}