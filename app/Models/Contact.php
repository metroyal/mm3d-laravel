<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model {
    protected $primaryKey = 'id';
    protected $table = 'contactSettings';
    protected $fillable = ['icon_code','url','is_active'];
}