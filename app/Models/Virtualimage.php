<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Virtualimage extends Model {
    protected $primaryKey = 'id';
    protected $table = 'panorama';
    protected $fillable = ['id_project','file_name'];
}