<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class RegisterUser extends Model {
    protected $primaryKey = 'id';
    protected $table = 'users';
    protected $fillable = ['id','email','name','password','api_token','is_active'];
}