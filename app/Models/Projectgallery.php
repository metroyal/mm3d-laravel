<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Projectgallery extends Model {
    protected $primaryKey = 'id';
    protected $table = 'gallery';
    protected $fillable = ['id','id_project','file_name','is_active'];
}