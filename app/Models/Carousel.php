<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Carousel extends Model {
    protected $primaryKey = 'id';
    protected $table = 'carousel';
    protected $fillable = ['id','url_img','url_thumb','is_active','order_number'];
}