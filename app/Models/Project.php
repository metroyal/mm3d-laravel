<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project extends Model {
    protected $primaryKey = 'id_project';
    protected $table = 'project';

    // create default value for is_deleted
    protected $attributes = [
        'is_deleted' => 0
    ];
    
    protected $fillable = [
        'project_name',
        'client_name',
        'project_date_start', 
        'id_category', 
        'description',
        'youtube_link',
        'location',
        'thumb_project', 
        'header_image', 
        'use360',
        'is_deleted',
        'is_active'
    ];
}