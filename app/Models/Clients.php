<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model {
    protected $primaryKey = 'id';
    protected $table = 'clients';
    protected $fillable = ['id','name','img_file','img_thumb_file','order_number','is_deleted','is_active'];
}