<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\Category;

class ApiController extends Controller
{
    public function getProjects() {
        $result = Project::where('project.is_deleted','=','0')
                    ->where('project.is_active','=','1')
                    ->where('category.is_deleted','=','0')
                    ->leftJoin('category', 'project.id_category', '=', 'category.id_category')
                    ->select('project.*', 'category.category_name', 'category.category_type', 'category.class')
                    ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $result
        ]);
    }

    public function getProjectsByCategory($category) {
        $getCategoryByClassName = Category::where('class', '=', $category)->first();
        $categoryId = $getCategoryByClassName->id_category;

        $result = Project::where('project.is_deleted','=','0')
                    ->where('project.is_active','=','1')
                    ->where('category.is_deleted','=','0')
                    ->where('category.id_category','=',$categoryId)
                    ->leftJoin('category', 'project.id_category', '=', 'category.id_category')
                    ->select('project.*', 'category.category_name', 'category.category_type', 'category.class')
                    ->get();

        if(count($result)) {
            return response()->json([
                'status' => 200,
                'data' => $result
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'data' => $result,
                'message' => 'not found'
            ]);
        }
    }

    public function getCategories() {
        $result = Category::where('is_deleted','=','0')->get();
        
        return response()->json([
            'status' => 200,
            'data' => $result
        ]);
    }
}
