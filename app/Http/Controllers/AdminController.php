<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Validator;
use App\Entities\Models\User;

use App\Models\Project;
use App\Models\RegisterUser;
use App\Models\Carousel;
use App\Models\Category;
use App\Models\Clients;
use App\Models\Contact;
use App\Models\Virtualimage;
use App\Models\Projectgallery;
use App\Models\Testimonial;
use \FileUploader;
use File;
use Mail;

use DB;

use Carbon\Carbon;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Validation\ValidationException;

use Auth;

use App\Mail\MailToAdmin;
use App\Mail\MailToSender;

class AdminController extends Controller
{
    public function home(){
        $totalproject = Project::select()->get();
        $counttotalproject = $totalproject->count();

        $totalcarousel = Carousel::select()
                        ->where('is_deleted','=',null)
                        ->orWhere('is_deleted','=','0')
                        ->get();
        $counttotalcarousel = $totalcarousel->count();

        $categories = Category::where('is_deleted','=',null)
                        ->orWhere('is_deleted','=','0')
                        ->select('id_category', 'class', 'category_name')
                        ->get();
        
        $projects = DB::table('category')
            ->leftJoin('project', 'project.id_category', '=', 'category.id_category')
            ->select('project.*', 'category.id as cat_id', 'category.class', 'category.category_name', 'category.category_type')
            ->where('project.is_active', '=', '1')
            ->where('project.is_deleted', '=', '0')
            ->orderBy('project.created_at', 'desc')
            ->limit(60)
            ->get();
        
        $carousels = Carousel::where('is_active','=','1')
                        ->where('is_deleted','=',null)
                        ->orWhere('is_deleted','=','0')
                        ->orderBy('order_number','asc')
                        ->get();

        $testimonials = Testimonial::where('is_active','=','1')
                        ->where('is_deleted','=',null)
                        ->orWhere('is_deleted','=','0')
                        ->orderBy('order_number','asc')
                        ->get();

        $socials = Contact::where('is_active','=','1')
                        ->orderBy('id','asc')
                        ->get();

        $clients = Clients::where('is_active','=','1')
                        ->where('is_deleted','=','0')
                        ->orderBy('order_number','asc')
                        ->get();

        
        return view('home')
            ->with('projects', $projects)
            ->with('categories', $categories)
            ->with('carousels', $carousels)
            ->with('testimonials', $testimonials)
            ->with('clients', $clients)
            ->with('socials', $socials);
    }

    public function dashboard(){
        $totalproject = Project::select()
                        ->where('is_deleted','=','0')
                        ->get();
        $counttotalproject = $totalproject->count();

        $totalcarousel = Carousel::select()
                        ->where('is_deleted','=',null)
                        ->orWhere('is_deleted','=','0')
                        ->get();
        $counttotalcarousel = $totalcarousel->count();

        $mytime = Carbon::now();

        $projects = Project::where('is_deleted','=','0')->get();
        return view('admin/dashboard/dashboard', compact('counttotalproject','counttotalcarousel','mytime'))->with('projects', $projects);
    }

    public function getContactProfile(){
        $getDataContacts = Contact::all();
        return view('admin/dashboard/contact')->with('getDataContacts', $getDataContacts);
    }

    // set contact profile to active or inactive
    public function setContactActiveStatus(Request $request) {
        $contact = Contact::findOrFail($request->id);
    
        $contact->is_active = !$contact->is_active;
        $contact->save();
    
        return response()->json([
            'code' => 200,
            'message' => 'Status Updated'
        ], 200);
    }

    // deleteContact
    public function deleteContact(Request $request) {
        $contact = Contact::findOrFail($request->id);
        $contact->delete();
    
        return response()->json([
            'code' => 200,
            'message' => 'Contact Deleted'
        ], 200);
    }

    public function addContactProfile(Request $request){
        $validator = Validator::make($request->all(), [
            'iconCode'=>'required',
            'url'=>'required',
        ]);
        
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        else {
            Contact::create([
                'icon_code' => $request->get('iconCode'),
                'url' => $request->get('url')
            ]);
            session()->flash('success','New Contact Added');
            return redirect()->back();
        }
    }

    public function storeEditContactProfile(Request $r, $id){
        if($id != null) {
            $validator = Validator::make($r->all(), [
                'iconCode_edit'=>'required',
                'urlEdit'=>'required',
            ]);
            if ($validator->fails()) {
                session()->flash('warning', $validator->messages()->first());
                return redirect()->back()->withInput();
            } else {
                $result = Contact::find($id);
                $result->icon_code = $r->get('iconCode_edit');
                $result->url = $r->get('urlEdit');
                $result->save();
                session()->flash('success','Data Updated');
                return response()->json([
                    'status' => 'ok',
                    'message' => 'updated'
                ]);
            }
        }
    }

    public function getEditContactProfile($id) {
        if($id != null) {
            $result = Contact::find($id);
            $icon = $result->icon_code;
            $url = $result->url;

            return response()->json([
                'icon' => '<i class="'.$icon.' removeThis"></i>',
                'class' => $icon,
                'url' => $url
            ]);
        }
    }

    public function category(Request $r) {
        $categories = Category::where('is_deleted','=',null)
            ->orWhere('is_deleted','=','0')
            ->get();
        return view('admin/dashboard/category')->with('categories', $categories);
    }

    public function signin(){
        return view('admin/dashboard/login');
    }

    public function register(){
        return view('admin/dashboard/register');
    }

    public function logout() {
        auth()->logout();
        return redirect('/admin/login');
    }
    
    public function forgotpass(){
        return view('admin/dashboard/forgot-password');
    }

    public function projectlist(){
        $categories = Category::where('is_deleted','=','0')
                    ->orderBy('category_name', 'asc')
                    ->get();
        $projects = Project::where('project.is_deleted','=','0')
                    ->where('category.is_deleted','=','0')
                    ->leftJoin('category', 'project.id_category', '=', 'category.id_category')
                    ->select('project.*', 'category.category_name')
                    ->get();

        $activeproject = Project::where('is_active','=','1')
                            ->where('is_deleted','=','0')
                            ->get();
        $countactiveproject = $activeproject->count();

        $inactiveproject = Project::where('is_active','=','0')
                            ->where('is_deleted','=','0')
                            ->get();
        $countinactiveproject = $inactiveproject->count();
        
        return view('admin/dashboard/projects', compact('countactiveproject','countinactiveproject'))
                ->with('projects', $projects)
                ->with('categories', $categories);
    }

    public function projectDetail($id_project){
        $projectExists = Project::where('id_project', $id_project)->exists();
        
        if($projectExists == false) {
            abort_unless($projectExists, 403, 'Project Not Found');
        } else {
            $project = Project::where('project.id_project', $id_project)
            ->join('panorama', 'project.id_project', '=', 'panorama.id_project')
            ->first();
            if($project == null)
            {
                $project = Project::find($id_project);
            }
            $galleries = Projectgallery::where('gallery.id_project', $id_project)
                ->get();
            return view('projectview')
                ->with('project', $project)
                ->with('galleries', $galleries);
        }
    }
    
    public function addproject(){
        $categories = DB::table('category')
            ->orderBy('category_name', 'asc')
            ->get();
        return view('admin/dashboard/addproject')->with('categories', $categories);
    }

    public function editproject($id_project){
        $project = Project::where('project.id_project', $id_project)
            ->join('category', 'project.id_category', '=', 'category.id_category')
            ->select('project.*', 'category.id as cat_id', 'category.category_type')
            ->first();
        if($project == null)
        {
            $project = Project::find($id_project);
        }

        $categories = DB::table('category')
            ->orderBy('category_name', 'asc')
            ->get();

        $panoimage = VirtualImage::where('panorama.id_project', $id_project)
            ->first();

        $galleries = Projectgallery::where('gallery.id_project', $id_project)
            ->get();

        return view('admin/dashboard/editproject')
            ->with('galleries', $galleries)
            ->with('project', $project)
            ->with('panoimage', $panoimage)
            ->with('categories', $categories);
    }

    public function storeEditProject(Request $request, $id) {
        if ($id != null) {
            $categorytype = $request->get('categorytype');
            $validator = $this->validateEditProject($request, $categorytype);
    
            if ($validator->fails()) {
                session()->flash('warning', $validator->messages()->first());
                return redirect()->back()->withInput();
            }
    
            $result = Project::find($id);
            if (!$result) {
                session()->flash('warning', 'Project not found!');
                return redirect()->back();
            }
    
            if ($request->hasFile('thumbProject')) {
                $getFile = $request->file('thumbProject');
                $extension = $getFile->getClientOriginalExtension();
                $fileNameBig = 'bigProj_' . strtotime("now") . '.' . $extension;
                $fileNameThumb = 'thumbProj_' . strtotime("now") . '.' . $extension;
    
                Storage::put('public/project/' . $fileNameBig, file_get_contents($getFile));
    
                $imageThumb = $this->updateProjectImage($getFile);
                Storage::put('public/project/thumb/' . $fileNameThumb, $imageThumb->__toString());
    
                $result->thumb_project = $fileNameThumb;
                $result->header_image = $fileNameBig;
            }
    
            $result->project_name = $request->get('projectname');
            $result->id_category = $request->get('category');
            $result->youtube_link = $request->get('youtubeLink');
            $result->use360 = $request->has('use360') ? true : false;
            $result->save();
    
            session()->flash('success', 'Project Updated!');
            return redirect()->back();
        }
    }
    
    private function validateEditProject(Request $request, $categorytype)
    {
        switch ($categorytype) {
            case 'image':
                return Validator::make($request->all(), [
                    'projectname' => 'required',
                    'thumbProject' => 'required|image|max:1048'
                ]);
            case 'video':
                return Validator::make($request->all(), [
                    'projectname' => 'required',
                    'youtubeLink' => 'required',
                    'thumbProject' => 'required|image|max:1048'
                ]);
            default:
                return Validator::make([], []); // Invalid categorytype, no validation rules
        }
    }
    
    private function updateProjectImage($getFile)
    {
        $dataImage = getimagesize($getFile);
        $getWidth = $dataImage[0];
        $getHeight = $dataImage[1];
        $width = 600;
        $height = 300;
        $scale = max($width / $getWidth, $height / $getHeight);
        $w = ceil($getWidth * $scale);
        $h = ceil($getHeight * $scale);
        $x = ceil(($w - $width) / 2);
        $y = ceil(($h - $height) / 2);
    
        return Image::make($getFile)->resize($w, $h)->crop(535, 300, $x, $y)->encode('jpg');
    }

    public function removeProject(Request $r) {
        $result = Project::find($r->id);

        $url = 'public/project/'.$result->header_image;
        $urlThumb = 'public/project/thumb/'.$result->thumb_project;
        
        Storage::delete($url);
        Storage::delete($urlThumb);
        
        if($result->is_deleted == 0 || $result->is_deleted == null){
            $result->is_deleted='1';
            $result->save();
            return response()->json([
                'status' => 'ok',
                'message' => 'Project is successfully deleted!'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Already Deleted'
            ]);
        };
    }

    public function addprojectimage($id_project){
        $project = Project::where('project.id_project', $id_project)
            ->first();
        if($project == null)
        {
            $project = Project::find($id_project);
        }

        $galleries = Projectgallery::where('gallery.id_project', $id_project)
            ->get();

        return view('admin/dashboard/addprojectimage')
            ->with('project', $project)
            ->with('galleries', $galleries);
    }

    public function newcategory(){
        $categories = Category::all();
        return view('admin/dashboard/addcategory')->with('categories', $categories);
    }

    public function addcarousel(){
        return view('admin/dashboard/addcarousel');
    }

    public function carousel(){
        $images = Carousel::orderBy('order_number', 'ASC')
            ->where('is_deleted','=',null)
            ->orWhere('is_deleted','=','0')
            ->get();
        return view('admin/dashboard/carousel')->with('images', $images);
    }

    public function setCarouselOrder(Request $request) {
        // $result = $request->positions;
        // dd($result[0]); exit;
        foreach($request->positions as $position) {
            $index = $position[0];
            $newPosition = $position[1];
            $affectedRows = Carousel::whereId($index)->update
            (['order_number' => $newPosition]);
        }
    }

    public function viewTestimonial(){
        $testimonials = Testimonial::orderBy('order_number', 'ASC')
                        ->where('is_deleted','=',null)
                        ->orWhere('is_deleted','=','0')
                        ->get();
        return view('admin/dashboard/testimonial')->with('testimonials', $testimonials);
    }

    public function viewClient() {
        $clients = Clients::orderBy('order_number', 'ASC')
            ->where('is_deleted','=',null)
            ->orWhere('is_deleted','=','0')
            ->get();
        return view('admin/dashboard/clients')->with('clients', $clients);
    }

    public function UploadClientImage(Request $request){
        $id = $request->idDataToSave;
        $imgFile = $request->fileNameToSave;
        $url = 'public/clients/'.$imgFile;
        $urlThumb = 'public/clients/thumb/thumb_'.$imgFile;
        
        if (request()->hasFile('file')) {
            Storage::delete($url);
            Storage::delete($urlThumb);
            $getFile = $request->file('file');
            $extension = $getFile->getClientOriginalExtension();
            $fileNameOriginal = strtotime("now").'.'.$extension;
            $fileNameThumb = 'thumb_'.strtotime("now").'.'.$extension;
            $image = Image::make($getFile);
            Storage::put('public/clients/'.$fileNameOriginal, file_get_contents($getFile));    
            $dataImage = getimagesize($getFile);
            $getWidth = $dataImage[0];
            $getHeight = $dataImage[1];
            $width = 400;
            $height = 400;
            $scale = max($width / $getWidth, $height / $getHeight);
            $w = ceil($getWidth * $scale);
            $h = ceil($getHeight * $scale);
            $x = ceil(($w - $width) / 2);
            $y = ceil(($h - $height) / 2);
            $imageThumb = Image::make($getFile)->resize($w, $h)->crop(400, 400)->encode('png');
            Storage::put('public/clients/thumb/'.$fileNameThumb, $imageThumb->__toString());

            $client = new Clients;
            $client->where('id', $id)->update([
                'img_file' => $fileNameOriginal,
                'img_thumb_file' => $fileNameThumb,
                'is_active' => '1'
            ]);

            session()->flash('success','Success');

            return response()->json([
                'status' => 'ok',
                'message' => 'updated'
            ]);
        }
    }

    public function addClient(Request $request) {
        $validator = Validator::make($request->all(), [
            'clientName'=>'required'
        ]);
        
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        else {
            $lastOrder = DB::table('clients')->latest()->first('id');
            
            if($lastOrder == null) {
                $orderNumber = 1;
            } else {
                $lastOrderValue = get_object_vars($lastOrder)['id'];
                $orderNumber = $lastOrderValue + 1;
            }

            $client = new Clients;
            $client->name = $request->clientName;
            $client->order_number = $orderNumber;
            $client->save();
            $idRow = $client->id;

            return response()->json([
                'status' => 'ok',
                'idRow' => $idRow 
            ]);
        }
    }

    public function getClientById($id) {
        $client = Clients::find($id);
        return view('admin/dashboard/editclient')->with('client', $client);
    }

    public function updateClient(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'clientName'=>'required'
        ]);

        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        else {
            $client = Clients::find($id);
            if($client->name != $request->clientName) {
                $client = new Clients;
                $client->where('id', $id)->update([
                    'name' => $request->clientName
                ]);
                session()->flash('success','Client Updated');
            }
        }

        return response()->json([
            'status' => 'ok',
            'idRow' => $id,
            'imgFile' => $client->img_file
        ]);
    }

    public function deleteClient(Request $r) {
        $result = Clients::find($r->id);

        $url = 'public/clients/'.$result->img_file;
        $urlThumb = 'public/clients/thumb/'.$result->img_thumb_file;
        
        Storage::delete($url);
        Storage::delete($urlThumb);
            
        if($result->is_deleted == 0 || $result->is_deleted == null){
            $result->is_deleted='1';
            $result->save();
        } else {
            echo('Already Deleted');
        };
    }

    public function setClientOrder(Request $request) {
        foreach($request->positions as $position) {
            $index = $position[0];
            $newPosition = $position[1];
            $affectedRows = Clients::whereId($index)->update
            (['order_number' => $newPosition]);
        }
    }

    public function toggleClientActive(Request $r) {
        $result = Clients::find($r->id);
        if($result->is_active == 0){
            $result->is_active='1';
            $result->save();
        } else {
            $result->is_active='0';
            $result->save();
        };
    }

    public function addTestimonial(Request $request){
        $validator = Validator::make($request->all(), [
            'clientname'=>'required',
            'testimonial'=>'required',
            'range_testi'=>'required|numeric|between:1,5|not_in:0',
        ]);
        
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        else {
            $lastOrder = DB::table('testimonial')->latest()->first('id');
            // echo($lastOrder);
            // return;
            
            if($lastOrder == null) {
                $orderNumber = 1;
            } else {
                $lastOrderValue = get_object_vars($lastOrder)['id'];
                $orderNumber = $lastOrderValue + 1;
            }
            Testimonial::create([
                'name' => $request->get('clientname'),
                'testimonial' => $request->get('testimonial'),
                'rate' => $request->get('range_testi'),
                'order_number' => $orderNumber
            ]);
            session()->flash('success','New Testimonial Added');
            return redirect()->back()->withInput();
        }
    }

    public function getEditTestimonial($idTesti) {
        $testi = Testimonial::where('testimonial.id', $idTesti)
            ->first();
        if($idTesti != null) {
            $result = Testimonial::find($idTesti);
            $name = $result->name;
            $testi = $result->testimonial;
            $rate = $result->rate;

            return response()->json([
                'name' => $name,
                'testi' => $testi,
                'rate' => $rate
            ]);
        }
    }

    public function updateTestimonial(Request $r, $idTesti) {
        $testi = Testimonial::where('testimonial.id', $idTesti)
            ->first();
        if($idTesti != null)
        {
            $validator = Validator::make($r->all(), [
                'clientnameEdit'=>'required',
                'testimonialEdit'=>'required',
                'range_testiEdit'=>'required|numeric|between:1,5|not_in:0',
            ]);
            if ($validator->fails()) {
                session()->flash('warning', $validator->messages()->first());
                return redirect()->back()->withInput();
            } else {
                $result = Testimonial::find($idTesti);
                $result->name = $r->get('clientnameEdit');
                $result->testimonial = $r->get('testimonialEdit');
                $result->rate = $r->get('range_testiEdit');
                $result->save();
                session()->flash('success','Testimonial Updated');
                return response()->json([
                    'status' => 'ok',
                    'message' => 'updated'
                ]);
            }
        }
    }

    public function setTestimonialOrder(Request $request) {
        foreach($request->positions as $position) {
            $index = $position[0];
            $newPosition = $position[1];
            $affectedRows = Testimonial::whereId($index)->update
            (['order_number' => $newPosition]);
        }
    }

    public function removeTestimonial(Request $request) {
        $result = Testimonial::find($request->id);
        if($result->is_deleted == 0 || $result->is_deleted == null){
            $result->is_deleted='1';
            $result->save();
        } else {
            echo('Already Deleted');
        };
    }

    public function submit(Request $request) {
        
        dd($request);
        return;

		// upload
        // $upload = $FileUploader->upload();
        // echo("success");
        // return;
    }

    public function removeFile(Request $request) {
		@unlink($_POST['image']);
		exit;
	}

    public function storeProject(Request $request)
    {
        $categorytype = $request->get('categorytype');

        $validator = $this->getValidator($request, $categorytype);

        if (!$validator) {
            session()->flash('warning', 'Set your project!');
            return redirect()->back();
        }

        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        if ($request->hasFile('thumbProject')) {
            $this->storeProjectFilesAndData($request);
            session()->flash('success', 'New Project Added Successfully!');
        }

        return redirect('admin/projects');
    }

    private function getValidator(Request $request, $categorytype)
    {
        switch ($categorytype) {
            case 'image':
                return Validator::make($request->all(), [
                    'projectname' => 'required',
                    'thumbProject' => 'required|image|max:1048'
                ]);
            case 'video':
                return Validator::make($request->all(), [
                    'projectname' => 'required',
                    'youtubeLink' => 'required',
                    'thumbProject' => 'required|image|max:1048'
                ]);
            default:
                return null;
        }
    }

    private function storeProjectFilesAndData(Request $request)
    {
        $getFile = $request->file('thumbProject');
        $extension = $getFile->getClientOriginalExtension();
        $fileNameBig = 'bigProj_' . strtotime("now") . '.' . $extension;
        $fileNameThumb = 'thumbProj_' . strtotime("now") . '.' . $extension;

        Storage::put('public/project/' . $fileNameBig, file_get_contents($getFile));

        $image = Image::make($getFile);
        $imageThumb = $this->createThumbnail($getFile);
        Storage::put('public/project/thumb/' . $fileNameThumb, $imageThumb->__toString());

        $use360 = $request->has('use360') ? true : false;

        Project::create([
            'project_name' => $request->get('projectname'),
            'client_name' => $request->get('clientname'),
            'id_category' => $request->get('category'),
            'description' => $request->get('description'),
            'youtube_link' => $request->get('youtubeLink'),
            'location' => $request->get('projLocation'),
            'is_active' => '1',
            'thumb_project' => $fileNameThumb,
            'header_image' => $fileNameBig,
            'use360' => $use360
        ]);
    }

    private function createThumbnail($getFile)
    {
        $dataImage = getimagesize($getFile);
        $getWidth = $dataImage[0];
        $getHeight = $dataImage[1];
        $width = 600;
        $height = 300;
        $scale = max($width / $getWidth, $height / $getHeight);
        $w = ceil($getWidth * $scale);
        $h = ceil($getHeight * $scale);
        $x = ceil(($w - $width) / 2);
        $y = ceil(($h - $height) / 2);

        return Image::make($getFile)->resize($w, $h)->crop(535, 300, 25, 25)->encode('jpg');
    }

    public function registeruser(Request $request) {
        $validator = Validator::make($request->all(), [
            'email'=>'required',
            'name'=>'required',
            'password'=>'required|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        else {
            $request->validate([
                'email'=>'required',
                'name'=>'required',
                'password'=>'required'
            ]);
            RegisterUser::create([
                'email' => $request->get('email'),
                'name' => $request->get('name'),
                'password'=> bcrypt($request->password),
                'api_token' => str_random(100),
                'is_active' => '1'
            ]);
            
            return redirect('admin/login')->with(
                session()->flash('success','You are now registered, please login!')
            );
        }
    }

    public function setActive(Request $r) {
        $result = Project::find($r->id);
        if($result->is_active == 0){
            $result->is_active='1';
            $result->save();
        } else {
            $result->is_active='0';
            $result->save();
        };
    }

    public function setCarouselActive(Request $r) {
        $result = Carousel::find($r->id);
        if($result->is_active == 0){
            $result->is_active='1';
            $result->save();
        } else {
            $result->is_active='0';
            $result->save();
        };
    }

    public function removeCarousel(Request $r) {
        $result = Carousel::find($r->id);
        if($result->is_deleted == 0 || $result->is_deleted == null){
            $result->is_deleted='1';
            $result->save();
        } else {
            echo('Already Deleted');
        };
    }

    public function dologin(Request $request) {
        $validator = Validator::make($request->all(), [
            'email'=>'required|email|exists:users',
            'password'=>'required',
        ]);
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        else {
            $request->validate([
                'email'=>'required',
                'password'=>'required'
            ]);
            if(auth::attempt(
                    ['email' => $request->get('email'),
                    'password'=> $request->get('password')
                    ])
                )
            {
                return redirect('admin')->with(
                    session()->flash('success','You are logged in!')
                );
            } else {
                return redirect('admin/login')->with(
                    session()->flash('warning','Failed!')
                );
            }
        }
    }

    public function uploadImgCarousel($imageData, $file, $getFile){
        $fileOri = strtotime("now").'.'.$getFile->extension();
        $fileName = 'thumb_'.strtotime("now").'.'.$getFile->extension();
        $image = Image::make($file);
        $path =  public_path();          
        $image->save($path."/cmsBSB/images/carousel/".$fileOri);
        // $image->crop(600, 350);
        // $image->resize(300, 200);
        $dataImage = getimagesize($file);
        $getWidth = $dataImage[0];
        $getHeight = $dataImage[1];
        $width = 400;
        $height = 200;
        $scale = max($width / $getWidth, $height / $getHeight);
        $w = ceil($getWidth * $scale);
        $h = ceil($getHeight * $scale);
        $x = ceil(($w - $width) / 2);
        $y = ceil(($h - $height) / 2);
        $image->resize($w, $h);
        $image->save($path."/cmsBSB/images/carousel/".$fileName);
        $lastOrder = DB::table('carousel')->latest()->first('id');
        $lastOrderValue = get_object_vars($lastOrder)['id'];
        Carousel::create([
            'url_img' => $fileOri,
            'url_thumb' => $fileName,
            'is_active' => '1',
            'order_number' => $lastOrderValue + 1
        ]);
    }

    public function UploadCarousel(Request $r){
        $validator = Validator::make($r->all(), [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1048'
        ]);
        echo $validator;
        return;

        if ($validator->fails()) {
            return redirect('admin/newcarousel')->with(
                session()->flash('error',$validator->messages()->first())
            );
        } else if (request()->hasFile('file')) {
            $this->validate($r, [
                'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1048',
            ]);
            
            $file = $r->file('file');

            dd($file);
            return

            // $fileName = $this->uploadImgCarousel('images', $file, $r->images);
            $fileOri = strtotime("now").'.'.$getFile->extension();
            $fileName = 'thumb_'.strtotime("now").'.'.$getFile->extension();
            $image = Image::make($file);
            $path =  public_path();          
            $image->save($path."/cmsBSB/images/carousel/".$fileOri);
            // $image->crop(600, 350);
            // $image->resize(300, 200);
            $dataImage = getimagesize($file);
            $getWidth = $dataImage[0];
            $getHeight = $dataImage[1];
            $width = 400;
            $height = 200;
            $scale = max($width / $getWidth, $height / $getHeight);
            $w = ceil($getWidth * $scale);
            $h = ceil($getHeight * $scale);
            $x = ceil(($w - $width) / 2);
            $y = ceil(($h - $height) / 2);
            $image->resize($w, $h);
            $image->save($path."/cmsBSB/images/carousel/".$fileName);
            $lastOrder = DB::table('carousel')->latest()->first('id');
            $lastOrderValue = get_object_vars($lastOrder)['id'];
            Carousel::create([
                'url_img' => $fileOri,
                'url_thumb' => $fileName,
                'is_active' => '1',
                'order_number' => $lastOrderValue + 1
            ]);
            
            return redirect('admin/slideshow')->with(
                session()->flash('success','Image Uploaded')
            );
        } 
    }

    public function UploadCarouselTest(Request $request){
        if (request()->hasFile('image')) {
            $getFile = $request->file('image');
            // dd($file);
            // return;
            $extension = $getFile->getClientOriginalExtension();
            $fileNameBig = strtotime("now").'.'.$extension;
            $fileNameThumb = 'thumb_'.strtotime("now").'.'.$extension;
            $image = Image::make($getFile);
            Storage::put('public/carousel/'.$fileNameBig, file_get_contents($getFile));
            // $path =  public_path();
            // $image->save($path."/cmsBSB/images/carousel/".$fileNameBig);       
            $dataImage = getimagesize($getFile);
            $getWidth = $dataImage[0];
            $getHeight = $dataImage[1];
            $width = 400;
            $height = 200;
            $scale = max($width / $getWidth, $height / $getHeight);
            $w = ceil($getWidth * $scale);
            $h = ceil($getHeight * $scale);
            $x = ceil(($w - $width) / 2);
            $y = ceil(($h - $height) / 2);
            // $image->resize($w, $h);
            // $image->crop(285, 200, 25, 25);
            // $image->save($path."/cmsBSB/images/carousel/".$fileNameThumb);
            $imageThumb = Image::make($getFile)->resize($w, $h)->crop(326, 200, 25, 25)->encode('jpg');
            Storage::put('public/carousel/'.$fileNameThumb, $imageThumb->__toString());
            
            $lastOrder = DB::table('carousel')->latest()->first('id');
            
            if($lastOrder == null) {
                $orderNumber = 1;
            } else {
                $lastOrderValue = get_object_vars($lastOrder)['id'];
                $orderNumber = $lastOrderValue + 1;
            }
            Carousel::create([
                'url_img' => $fileNameBig,
                'url_thumb' => $fileNameThumb,
                'is_active' => '1',
                'order_number' => $orderNumber
            ]);
            
            return redirect('admin/slideshow')->with(
                session()->flash('success','Image Uploaded')
            );
        } 
        // $validator = Validator::make($r->all(), [
        //     'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1048'
        // ]);
        // if ($validator->fails()) {
        //     return redirect('admin/newcarousel')->with(
        //         session()->flash('error',$validator->messages()->first())
        //     );
        // } else 
    }

    public function addnewcategory(Request $r){
        $validator = Validator::make($r->all(), [
            'categoryName'=>'required',
            'categoryType'=>'required'
        ]);
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            return redirect()->back();
        }
        else {
            $cat_id = DB::table('category')->max('id');
            if($cat_id == null) {
                $cat_id = '0';
            }
            $categoryName = $r->get('categoryName');
            $categoryType = $r->get('categoryType');
            $lowercase = strtolower($categoryName);
            $ctgrNoSpace = preg_replace('/\s+/', '', $lowercase);
            Category::create([
                'id_category' => $cat_id+1,
                'category_type' => $categoryType,
                'category_name' => $categoryName,
                'class' => $ctgrNoSpace
            ]);
            
            return redirect('admin/newcategory')->with(
                session()->flash('success','New Category Added')
            );
        }

        
    }

    public function getCategoryById($idCategory) {
        $category = Category::where('category.id_category', $idCategory)
            ->first();
        if($idCategory != null) {
            $result = Category::find($idCategory);
            $name = $result->category_name;
            $type = $result->category_type;

            return response()->json([
                'name' => $name,
                'type' => $type
            ]);
        }
    }

    public function updateCategory(Request $r, $idCategory) {
        if($idCategory != null) {
            $validator = Validator::make($r->all(), [
                'categoryName'=>'required',
                'categoryType'=>'required'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'errors',
                    'message' => $validator->messages()
                ]);
            }
            else { 
                $result = Category::find($idCategory);
                $categoryNameFromDB = $result->category_name;
                $newCategoryName = $r->get('categoryName');

                $categoryTypeFromDB = $result->category_type;
                $newCategoryType = $r->get('categoryType');

                if($categoryNameFromDB == $newCategoryName && $categoryTypeFromDB == $newCategoryType) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Nothing change on Category'
                    ]);
                } else {
                    $lowercase = strtolower($newCategoryName);
                    $ctgrNoSpace = preg_replace('/\s+/', '', $lowercase);

                    $result->category_name = $newCategoryName;
                    $result->class = $ctgrNoSpace;
                    $result->category_type = $newCategoryType;
                    $result->save();
                    session()->flash('success','Category is Updated');
                    return response()->json([
                        'status' => 'ok',
                        'message' => 'updated'
                    ]);
                }
            }
        }
    }

    public function removeCategory(Request $r) {
        $result = Category::find($r->id);
        if($result->is_deleted == 0 || $result->is_deleted == null){
            $result->is_deleted='1';
            $result->save();
        } else {
            echo('Already Deleted');
        };
    }

    public function actionprojectimage(Request $request) {
        if (request()->hasFile('image')) {
            $random = Str::random(10);
            $fileGallery = $request->file('image');
            $extension = $fileGallery->getClientOriginalExtension();
            $fileNameBig = $random.'.'.$extension;
            $fileNameThumb = 'thumb_'.$random.'.'.$extension;
            $image = Image::make($fileGallery);
            // $path =  url('/');
            // $save_path = $path."/public/storage/image-gallery/";
            // $save_path_thumb = $path."/public/storage/image-gallery/thumb/";
            // if (!file_exists($save_path)) {
            //     mkdir($save_path, 0755, true);
            // }
            // if (!file_exists($save_path_thumb)) {
            //     mkdir($save_path_thumb, 0755, true);
            // }
            Storage::put('public/image-gallery/'.$fileNameBig, file_get_contents($fileGallery));
            // $image->save($save_path.$fileNameBig);       
            $dataImage = getimagesize($fileGallery);
            $getWidth = $dataImage[0];
            $getHeight = $dataImage[1];
            $width = 400;
            $height = 200;
            $scale = max($width / $getWidth, $height / $getHeight);
            $w = ceil($getWidth * $scale);
            $h = ceil($getHeight * $scale);
            $x = ceil(($w - $width) / 2);
            $y = ceil(($h - $height) / 2);
            $imageThumb = Image::make($fileGallery)->resize($w, $h)->crop(326, 200, 25, 25)->encode('jpg');
            Storage::put('public/image-gallery/thumb/'.$fileNameThumb, $imageThumb->__toString());
            //$image->save($save_path_thumb.$fileNameThumb);
            $idproject = $request->get('idproject');

            Projectgallery::create([
                'id_project' => $idproject,
                'file_name' => $fileNameBig,
                'is_active' => '1'
            ]);
            
            return redirect('admin/editproject/'.$idproject)->with(
                session()->flash('success','Project Image Uploaded')
            );
        }
    }

    public function sortcarousel(Request $r) {
        $id = $r->id;
        $oldNumber = $r->oldNumber; 
        $newNumber = $r->newNumber;
        $nambah = 0;
        $mengurangi = 0;
        if ($oldNumber < $newNumber) {
            $i = $newNumber;
            $old = $oldNumber;
            while ($i >= $old) {
                $project = DB::table('carousel')
                ->where('order_number', $i)
                ->get();
                $id_carousel="";
                foreach($project as $data)
                {
                    $id_carousel = $data->id;
                    $setNomorUrut = $data->order_number-1;
                    // $hasil = $data->order_number - 1;
                    // if($mengurangi == 1) {
                    //     $hasil = $setNomorUrut - 1;
                    // }
                    // else if ($nambah == 1) {
                    //     $hasil = $setNomorUrut + 1;
                    // }
                    echo"$id_carousel | $setNomorUrut<br>";
                }

                if($i == $old){
                    $result = Carousel::find($id_carousel);
                    $result->order_number = $newNumber;
                    $result->save();
                    echo"Id: $id_carousel update terakhir ke $setNomorUrut<br>";
                }else{
                    $result = Carousel::find($id_carousel);
                    $result->order_number = $setNomorUrut;
                    $result->save();
                    echo"ID: $id_carousel update urut ke $setNomorUrut<br>";
                }
                $i--;
            }
        } else {
            $nambah = 1;
        }
    }

    public function sendEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'phone'=>'required',
            'email' => [
                'required',
                'regex:/^([a-z0-9+-]+)(.[a-z0-9+-]+)*@([a-z0-9-]+\.)+[a-z]{2,6}$/ix'
            ],
            'message'=>'required',
        ]);
        
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
            throw new ValidationException($validator);
        } else {
            $content = [
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'message' => $request->message
            ];

            try {
                Mail::to('muhammad@mm3darsitektur.com')->send(new MailToAdmin($content));
                Mail::to($request->email)->send(new MailToSender($content));
                
                response()->json([
                    'status' => 'OK',
                    'message' => 'Email is Sent!'
                ], 200);

                session()->flash('success','Your Mail is sent!');
                // return back()->with(
                //     session()->flash('success','Your mail is sent!')
                // );
            } catch (\Exception $e){
                // return response (['status' => false,'errors' => $e->getMessage()]);
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ], 400);
            }
        }
    }
}
